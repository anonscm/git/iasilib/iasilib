package fr.lri.iasi.libs.formallogic.strategy;

import fr.lri.iasi.libs.formallogic.Formula;

/**
 * The consequence finding strategy interface.
 * @author Andre Fonseca
 *
 * @param <C> the type of the query formula
 * @param <S> the type of the result formula set
 */
public interface ConsequenceFindingStrategy<T, S extends Formula<S>, C extends Formula<C>> {
	
	/**
	 * If this consequent finding strategy saturates the theory, indicates if the results
	 * returned will contain the whole modified theory or only the new consequents produced.
	 * @param onlyNewConsequents
	 */
	void onlyNewConsequentResults(boolean onlyNewConsequents);

	/**
	 * Executes the consequence finding routine.
	 * @param formulaToAdd to be used as query
	 * @param formulaAlreadyAdded in the query, already added to the theory, if any
	 * @return formula set resulted from the consequence finding
	 */
	S process(T theory, S formulaToAdd, S formulaAlreadyAdded);
}
