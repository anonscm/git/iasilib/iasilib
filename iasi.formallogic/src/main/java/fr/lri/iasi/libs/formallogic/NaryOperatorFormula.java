package fr.lri.iasi.libs.formallogic;

import java.util.Set;

/**
* A set of logic formulas connected by a n-ary operator.
* @author Andre Fonseca
* 
* @param <R> the type of this class (see explanation of the recursive generic parameter on {@link Formula})
* @param <F> the type of the interconnected formulas
*/
public interface NaryOperatorFormula<R extends Formula<R>, F> extends Formula<R>, Set<F> {
	
	/**
	* Runs a union operation over the current formula and the given one.
	*
	* @param formula
	* @return formula obtained from the union.
	*/
	R union(R formula);
	
	/**
	* Runs a distribution operation over the current formula and the given one.
	*
	* @param formula
	* @return formula obtained from the distribution.
	*/
	R distribution(R formula);
	
	/**
	 * Set this formula as a tautology or not due the addition or remotion of an element
	 * @param isSat
	 */
	void setTautology(boolean isTautology);
	
	/**
	 * Set this formula as sat or unsat due the addition or remotion of an element
	 * @param isSat
	 */
	void setSat(boolean isSat);

}
