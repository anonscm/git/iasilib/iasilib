package fr.lri.iasi.libs.formallogic.utils.instance;



/**
 * Describes a instance parser, which are used to parse
 * peer knowledge base profile to produce a PropositionalInstance
 * @author Leo Cazenille
 */
public interface InstanceParser {
    /**
     * Begin parsing a file/stuff
     */
    Instance parse();

    /**
     * Create a new file/stuff from a working instance
     * @param path Path to export instance to
     */
    void export(String path);
}
