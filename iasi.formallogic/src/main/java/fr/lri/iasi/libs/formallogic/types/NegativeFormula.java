package fr.lri.iasi.libs.formallogic.types;

import fr.lri.iasi.libs.formallogic.Formula;
import fr.lri.iasi.libs.formallogic.UnaryOperatorFormula;

/**
 * Represents a formula preceded by a negation.
 * 
 * @author Andre Fonseca
 *
 * @param <F> the type of this class (see explanation of the recursive generic parameter on {@link Formula})
 */
public interface NegativeFormula<F extends Formula<F>> extends UnaryOperatorFormula<F> {

	/**
	 * Apply negation rule to the current formula following the De Morgan's law.
	 * 
	 * @return the negative version of the current formula
	 */
	Formula<?> applyNegation();
}
