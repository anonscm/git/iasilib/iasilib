package fr.lri.iasi.libs.formallogic.utils.instance;

import java.util.Set;

import fr.lri.iasi.libs.formallogic.Formula;
import fr.lri.iasi.libs.formallogic.Indexable;

/**
* The instance builder element.
* @author Andre Fonseca
* 
* @param <I> the type of the instance to be built
* @param <P> the type of elements on the production set
* @param <T> the type of the theory contained in this instance
*/
public interface InstanceBuilder<I extends Instance,
								 P extends Formula<P>,
								 T extends Indexable<T, P, ?>> {
	
	/**
	* Add the given theory on the current instance.
	*
	* @param theory
	*/
	void addTheory(T theory);
	
	/**
	* Add the given set as the production set of the current instance.
	*
	* @param production set
	*/
	void addProduction(Set<P> productionSet);
	
	/**
	* Return the built instance.
	*
	* @return new instance
	*/
	I create();

}
