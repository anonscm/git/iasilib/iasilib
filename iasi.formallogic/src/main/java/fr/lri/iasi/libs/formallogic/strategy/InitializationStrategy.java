package fr.lri.iasi.libs.formallogic.strategy;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;

/**
 * The formula set initialization strategy interface.
 * @author Andre Fonseca
 *
 */
public interface InitializationStrategy<T, S extends NaryOperatorFormula<S, ?>> {
	
	/**
	 * Initializes the formula set
	 * @param <S> the type of the result formula set
	 * @return the expanded formula set
	 */
	S initialize(T theory);

}
