package fr.lri.iasi.libs.formallogic.utils.instance;

/**
 * Instance representing a set of theories
 * 
 * @author Andre Fonseca
 */
public interface Instance {
	
	/**
	* Returns the current instance name.
	*
	* @return current instance name.
	*/
	String getName();
	
	/**
	 * @return a copy of the current instance.
	 */
	Instance copy();

}
