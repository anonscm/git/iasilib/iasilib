package fr.lri.iasi.libs.formallogic;

/**
 * Represents a formula preceded by a unary operator.
 * 
 * @author Andre Fonseca
 *
 * @param <F> the type of this class (see explanation of the recursive generic parameter on {@link Formula})
 */
public interface UnaryOperatorFormula<F extends Formula<F>> extends Formula<F> {

}
