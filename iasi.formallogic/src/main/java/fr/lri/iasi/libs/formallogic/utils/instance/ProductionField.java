package fr.lri.iasi.libs.formallogic.utils.instance;

import java.io.Serializable;
import java.util.Map;

import fr.lri.iasi.libs.formallogic.Formula;
import fr.lri.iasi.libs.formallogic.Restriction;

/**
 * Describes the production field part of an instance.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formula where the restrictions will apply.
 */
public interface ProductionField<T extends Formula<T>> extends Serializable {
	
	/**
	 * @return all restrictions contained on the current production field.
	 */
	Map<String, Restriction<T>> getRestrictions();
	
	/**
	 * Add a restriction on the current production field.
	 * 
	 * @param restriction
	 */
	void addRestriction(Restriction<T> restriction);
	
	/**
	 * Removes a restriction on the current production field.
	 * 
	 * @param name : the key name of the chosen restriction.
	 */
	void removeRestriction(String name);
	
	/**
	 * @return a copy of the current production field.
	 */
	ProductionField<T> copy();

}
