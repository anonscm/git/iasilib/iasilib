package fr.lri.iasi.libs.formallogic;

import java.io.Serializable;

/**
 * Represents a restriction that can be applied over a formula.
 * 
 * @author Andre Fonseca
 *
 * @param <F> the type of the formula to be treated by this restriction.
 */
public interface Restriction<F extends Formula<F>> extends Serializable {
	
	/**
	 * @return the human-readable name of the current restriction.
	 */
	String getName();
	
	/**
	 * Discard operation over the given formula.
	 * 
	 * @param formula : formula to be restricted.
	 * @return true if the formula was restrained, false otherwise.
	 */
	boolean discard(F formula);

}
