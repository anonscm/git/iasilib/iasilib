package fr.lri.iasi.libs.formallogic.types;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;

/**
 * Represents a formula where its elements are interconnected by disjunctions ('or' operator).
 * 
 * @author Andre Fonseca
 *
 * @param <R> the type of this class (see explanation of the recursive generic parameter on {@link Formula})
 * @param <F> the type of the interconnected formulas
 */
public interface DisjunctionFormula<R extends NaryOperatorFormula<R,F>, F> extends NaryOperatorFormula<R,F> {
	
}
