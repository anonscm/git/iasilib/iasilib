package fr.lri.iasi.libs.formallogic.utils.instance;

import java.io.FileNotFoundException;


/**
 * An abstract implementation of an instance parser,
 * with base methods
 * @author Andre Fonseca
 * @author Leo Cazenille
 */
public abstract class AbstractInstanceParser implements InstanceParser {

    /* --- Properties --- */

    /** The path of the input XML file */
    protected final String path;

    /** Instance obtained from the parsing */
    protected Instance instance;

    /**
     * @param path Path of the input file
     */
    public AbstractInstanceParser(String path) throws FileNotFoundException {
        if (path == null) {
            throw new IllegalArgumentException("path");
        }

        this.path = path;
    }

    /* --- Accessors --- */
    public final String getPath() {
        return path;
    }

}
