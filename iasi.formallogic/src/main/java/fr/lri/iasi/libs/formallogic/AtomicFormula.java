package fr.lri.iasi.libs.formallogic;

/**
* A logic formula (expression).
* @author Andre Fonseca
*/
public interface AtomicFormula<F extends Formula<F>> extends Formula<F> {
	
	/**
	* @return a copy of the opposite form of this atomic formula.
	*/
	F opposite();

}
