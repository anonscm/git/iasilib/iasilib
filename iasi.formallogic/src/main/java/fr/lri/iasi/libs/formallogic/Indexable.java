package fr.lri.iasi.libs.formallogic;

import java.util.Collection;
import java.util.Map;

/**
 * Represents an indexable set of formulas. It handles vocabularies of the containing elements.
 * @author Andre Fonseca
 *
 * @param <T> the type of this class (see explanation of the generic parameter on {@link Formula})
 * @param <Index> the type of the elements used as indexes of formulas on a theory
 * @param <Words> the type of the indexed formulas
 */
public interface Indexable<T extends Indexable<T, Index, Words>,
						Index extends Formula<Index>, 
						Words extends Formula<Words>>  {
	
	/**
	 * @return the map between the canonical word and its current weak reference.
	 */
	Map<Words, Words> getCanonicalMap();
	
	/**
	 * @return an immutable view of the theory vocabulary
	 */
	Map<Index, Collection<Words>> getVocabulary();
	
	/**
	 * Return formulas associated with the parameter index (immutable).
	 * 
	 * @param index
	 * @return a view of the associated formulas
	 */
	Collection<Words> getVocabularyFor(Index index);
	
	/**
	 * Return true if there are some formulas associated with the given index. 
	 * 
	 * @param index
	 * @return true if there are formulas associated with the given index. Otherwise, return false.
	 */
	boolean isInVocabulary(Index index);
	
}
