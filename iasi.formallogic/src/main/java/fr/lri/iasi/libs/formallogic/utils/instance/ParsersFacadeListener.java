package fr.lri.iasi.libs.formallogic.utils.instance;



/**
 * This interface is used to deals when ParsersFacade has finished an
 *  analysis
 * @author Andre Fonseca
 * @author Leo Cazenille
 */
public interface ParsersFacadeListener<I> {
    /**
     * Called when the loading of the file is completed
     * @param the obtained Instance or null if there was a problem
     */
    void loadingCompleted(I propositionalInstance);
}
