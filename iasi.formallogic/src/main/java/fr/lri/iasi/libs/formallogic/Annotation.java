package fr.lri.iasi.libs.formallogic;

import java.io.Serializable;

/**
 * Represents a formula annotation.
 * 
 * @author Andre Fonseca
 */
public interface Annotation<A extends Annotation<A,F>, F> extends Serializable {
	
	/**
	* Returns the current annotation name.
	*
	* @return current annotation name.
	*/
	String getName();
	
	/**
	 * @return the typed content of the current annotation
	 */
	F getContent();
	
	/**
	 * Add content to the current annotation.
	 * @param content to be added
	 */
	void addContent(Object content);
	
	/**
	 * @return a copy of the current annotation.
	 */
	A copy();

}
