package fr.lri.iasi.libs.formallogic;

import java.io.Serializable;
import java.util.Map;

/**
 * An elements within a formula. It can be other formula or even a formula set. (Composite Pattern)
 * @author Andre Fonseca
 * 
 * @param <F> The same type of the formula element. Used to indicate to some generic operations that 
 * 			  the return type must be the same of the current class type.
 */
public interface Formula<F extends Formula<F>> extends Serializable {
	
	/**
	 * @return the symbol that represents the label of this formula
	 */
	String getLabel();
	
	/**
	 * Indicates whether the current formula is a tautology.
	 * 
	 * @return true if the current formula is a tautology. Otherwise, return false. 
	 */
	boolean isTautology();
	
	/**
	 * Indicates whether the current formula is a satisfiable.
	 * 
	 * @return true if the current formula is a satisfiable. Otherwise (if it is a contradiction), return false. 
	 */
	boolean isSat();
	
	/**
	 * Add the indicated annotation on the current formula.
	 * 
	 * @param annotation
	 */
	void addAnnotation(Annotation<?, ?> annotation);
	
	/**
	 * @return all annotations contained on the current formula.
	 */
	Map<String, Annotation<?, ?>> getAnnotations();
	
	/**
	 * @return a canonical copy (without annotations) of the current formula.
	 */
	F canonicalCopy();

	/**
	* @return a copy of the current formula.
	*/
	F copy();
	
}
