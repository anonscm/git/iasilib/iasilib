package fr.lri.iasi.libs.formallogic.types;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;

/**
 * Represents a formula where its elements are interconnected by conjunctions ('and' operator).
 * 
 * @author Andre Fonseca
 *
 * @param <R> the type of this class (see explanation of the recursive generic parameter on {@link Formula})
 * @param <F> the type of the interconnected formulas
 */
public interface ConjunctionFormula<R extends NaryOperatorFormula<R,F>, F> extends NaryOperatorFormula<R,F> {

	
}
