package fr.lri.iasi.somewhere.plogic


import org.specs.Specification
import org.specs.runner.ConsoleRunner
import org.specs.runner.JUnit4
import org.scalacheck.Prop._
import org.scalacheck.Prop


class SpecsBugSpecTest extends JUnit4(SpecsBugSpec)
object SpecsBugSpecRunner extends ConsoleRunner(SpecsBugSpec)

object SpecsBugSpec extends Specification with org.specs.ScalaCheck {

  "A String" should {
  /*
    "Have a good start" in {
      val prop = forAll{ (a:Int) => false } 
      prop must pass
    }
    */

    "Have a good start" in {
      val prop = forAll { (a: String, b: String) => 
        (a+b).startsWith(a)
      }
      prop must pass
    }
    "Have a good end" in {
      val prop = forAll { (a: String, b: String) => 
        (a+b).endsWith(b)
      }
      prop must pass
    }
    "Have a good substring 1" in {
      val prop = forAll { (a: String, b: String) => 
        (a+b).substring(a.length) == b
      }
      prop must pass
    }
    "Have a good substring 2" in {
      val prop = forAll { (a: String, b: String, c: String) => 
        (a+b+c).substring(a.length, a.length+b.length) == b
      }
      prop must pass
    }

  }

}

