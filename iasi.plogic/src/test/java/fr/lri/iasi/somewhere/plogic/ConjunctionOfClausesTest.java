package fr.lri.iasi.somewhere.plogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesImpl;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;

@RunWith(Parameterized.class)
public class ConjunctionOfClausesTest {
	
	/* --- PARAMETERS SETTINGS --- */ 
	public ConjunctionOfClausesTest(String dataType) {
		StrategiesConfigurator.getInstance().setDataType(dataType);
	}
	
	@Parameterized.Parameters
	public static List<Object[]> parameters() {
		return Arrays.asList(new Object[][] {
			      {"hashSet"},
			      {"trie"}
			    });
	}
	
	/* --- Tests --- */
	@Test
	public void testDistribution1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("C");
		builder2.addLiteral("D");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses paramConjunction = new ConjunctionOfClausesImpl();
		paramConjunction.add(clause3);
		
		ConjunctionOfClauses result = conjunction.distribution(paramConjunction);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("B");
		builder4.addLiteral("C");
		builder4.addLiteral("F");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("C");
		builder5.addLiteral("D");
		builder5.addLiteral("E");
		builder5.addLiteral("F");
		Clause clause5 = builder5.create();
		
		ConjunctionOfClauses toCompare = new ConjunctionOfClausesImpl();
		toCompare.add(clause4);
		toCompare.add(clause5);
		
		assertEquals(toCompare, result);
	}
	
	@Test
	public void testDistribution2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("E");
		builder2.addLiteral("D");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("E");
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses paramConjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause3);
		
		ConjunctionOfClauses result = conjunction.distribution(paramConjunction);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("B");
		builder4.addLiteral("E");
		builder4.addLiteral("F");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("D");
		builder5.addLiteral("E");
		builder5.addLiteral("F");
		Clause clause5 = builder5.create();
		
		ConjunctionOfClauses toCompare = new ConjunctionOfClausesImpl();
		conjunction.add(clause4);
		conjunction.add(clause5);
		
		assertEquals(toCompare, result);
	}

	@Test
	public void testRemove1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("C");
		builder1.addLiteral("D");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		builder2.addLiteral("C");
		builder2.addLiteral("E");
		builder2.addLiteral("F");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("B");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		conjunction.add(clause3);
		
		conjunction.remove(clause2);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("C");
		builder4.addLiteral("E");
		Clause clause4 = builder4.create();
		
		assertTrue(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
		assertTrue(conjunction.contains(clause3));
		assertFalse(conjunction.contains(clause4));
	}
	
	@Test
	public void testRemove2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("C");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		
		conjunction.remove(clause3);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
	}
	
	@Test
	public void testRemove3() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("A");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		
		conjunction.remove(clause2);
		
		assertTrue(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
	}
	
	@Test
	public void testEquals1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		ConjunctionOfClauses conjunction2 = new ConjunctionOfClausesImpl();
		conjunction2.add(clause2);
		
		assertTrue(conjunction.equals(conjunction2));
	}
	
	@Test
	public void testEquals2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("A");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		ConjunctionOfClauses conjunction2 = new ConjunctionOfClausesImpl();
		conjunction2.add(clause2);
		
		assertFalse(conjunction.equals(conjunction2));
	}
	
	@Test
	public void testSubsumes1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addLiteral("D");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("D");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		conjunction.add(clause3);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("D");
		builder4.addLiteral("E");
		Clause clause4 = builder4.create();
		
		assertFalse(conjunction.subsumes(clause4));
	}
	
	@Test
	public void testSubsumes2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addLiteral("D");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("D");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		conjunction.add(clause3);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("B");
		builder4.addLiteral("D");
		Clause clause4 = builder4.create();
		
		assertTrue(conjunction.subsumes(clause4));
	}
	
	@Test
	public void testSubsumes3() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addLiteral("D");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("E");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		conjunction.add(clause3);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("B");
		builder4.addLiteral("D");
		Clause clause4 = builder4.create();
		
		assertTrue(conjunction.subsumes(clause4));
	}
	
	@Test
	public void testSubsumes4() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addNegativeLiteral("B");
		Clause clause2 = builder2.create();
		
		assertTrue(conjunction.subsumes(clause2));
	}
	
	@Test
	public void testSubsumes5() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("B");
		Clause clause3 = builder3.create();
		
		Clause resultClause = clause2.resolve(clause3, new LiteralImpl("B", true));
		
		assertFalse(conjunction.subsumes(resultClause));
	}
		
	@Test
	public void testSubsumes6() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();

		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("B");
		Clause clause3 = builder3.create();
		
		Clause resultClause = clause2.resolve(clause3, new LiteralImpl("B", true));
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(resultClause);
		
		assertTrue(conjunction.subsumes(clause1));
	}
	
	@Test
	public void testAddSubsuming1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addLiteral("D");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("D");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		conjunction.add(clause3);
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("D");
		Clause clause4 = builder4.create();
		
		conjunction.addSubsuming(clause4);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause4));
		assertFalse(conjunction.contains(clause2));
		assertFalse(conjunction.contains(clause3));
	}
	
	
	@Test
	public void testAddSubsuming2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addNegativeLiteral("B");
		builder1.addLiteral("X");
		builder1.addNegativeLiteral("Y");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addNegativeLiteral("C");
		builder2.addLiteral("X");
		builder2.addNegativeLiteral("Y");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
	}
	
	@Test
	public void testAddSubsuming3() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("F");
		builder1.addLiteral("H");
		builder1.addLiteral("L");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("C");
		builder2.addNegativeLiteral("F");
		builder2.addLiteral("L");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
	}
	
	
	@Test
	public void testAddSubsuming4() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addNegativeLiteral("B");
		builder1.addLiteral("X");
		builder1.addNegativeLiteral("Y");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addNegativeLiteral("C");
		builder2.addLiteral("X");
		builder2.addNegativeLiteral("Y");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addNegativeLiteral("C");
		builder3.addLiteral("X");
		builder3.addNegativeLiteral("Y");
		Clause clause3 = builder3.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
		assertTrue(conjunction.contains(clause3));
	}
	
	@Test
	public void testAddSubsuming5() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addNegativeLiteral("B");
		builder1.addLiteral("X");
		builder1.addNegativeLiteral("Y");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addNegativeLiteral("C");
		builder2.addLiteral("X");
		builder2.addNegativeLiteral("Y");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addNegativeLiteral("C");
		builder3.addLiteral("X");
		builder3.addNegativeLiteral("Y");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("X");
		builder4.addNegativeLiteral("Y");
		Clause clause4 = builder4.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		
		assertFalse(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
		assertFalse(conjunction.contains(clause3));
		assertTrue(conjunction.contains(clause4));
	}
	
	@Test
	public void testAddSubsuming6() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("3");
		builder1.addLiteral("4");
		builder1.addLiteral("5");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("3");
		builder2.addLiteral("4");
		builder2.addLiteral("5");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		
		assertTrue(conjunction.contains(clause1));
	}
	
	@Test
	public void testAddSubsuming7() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		assertTrue(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
	}
	
	@Test
	public void testAddSubsuming8() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addNegativeLiteral("A");
		Clause clause1 = builder1.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		assertFalse(conjunction.add(clause1));
	}
	
	@Test
	public void testAddSubsuming9() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("B");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("B");
		Clause clause3 = builder3.create();
		
		Clause resultClause = clause2.resolve(clause3, new LiteralImpl("B", true));
		conjunction.add(resultClause);
		
		assertFalse(conjunction.contains(clause1));
		assertTrue(conjunction.contains(resultClause));
	}
	
	@Test
	public void testAddSubsuming10() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("A");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.add(clause2);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
	}
	
	@Test
	public void testAddSubsuming11() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		Clause clause2 = builder2.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		
		assertFalse(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
	}
		
	@Test
	public void testRemoveSubsumed1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("2");
		builder1.addLiteral("4");
		builder1.addLiteral("5");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("3");
		builder2.addNegativeLiteral("5");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("2");
		builder3.addLiteral("3");
		builder3.addLiteral("4");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("1");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("2");
		builder5.addNegativeLiteral("5");
		Clause clause5 = builder5.create();
		
		ClauseBuilder builder6 = new ClauseBuilder();
		builder6.addNegativeLiteral("2");
		builder6.addNegativeLiteral("3");
		builder6.addLiteral("5");
		Clause clause6 = builder6.create();
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("2");
		builder7.addLiteral("4");
		builder7.addLiteral("5");
		Clause clause7 = builder7.create();
		
		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		conjunction.addSubsuming(clause5);
		conjunction.addSubsuming(clause6);
		conjunction.removeSubsumed(clause7);
		
		assertFalse(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
		assertTrue(conjunction.contains(clause3));
		assertTrue(conjunction.contains(clause4));
		assertTrue(conjunction.contains(clause5));
		assertTrue(conjunction.contains(clause6));
	}
	
	@Test
	public void testRemoveSubsumed2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		builder1.addLiteral("D");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("B");
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("G");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("A");
		builder5.addLiteral("B");
		builder5.addLiteral("C");
		Clause clause5 = builder5.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		conjunction.removeSubsumed(clause5);
		
		assertFalse(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
		assertTrue(conjunction.contains(clause3));
		assertTrue(conjunction.contains(clause4));
	}
	
	@Test
	public void testRemoveSubsumed3() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		builder1.addLiteral("D");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("B");
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("G");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("A");
		builder5.addLiteral("B");
		Clause clause5 = builder5.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		conjunction.removeSubsumed(clause5);
		
		assertFalse(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
		assertFalse(conjunction.contains(clause3));
		assertTrue(conjunction.contains(clause4));
	}
	
	@Test
	public void testRemoveSubsumed4() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		builder1.addLiteral("D");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("B");
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("G");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("A");
		builder5.addLiteral("C");
		Clause clause5 = builder5.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		conjunction.removeSubsumed(clause5);
		
		assertFalse(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
		assertTrue(conjunction.contains(clause3));
		assertTrue(conjunction.contains(clause4));
	}
	
	@Test
	public void testRemoveSubsumed5() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		builder1.addLiteral("D");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("B");
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("G");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("A");
		builder5.addLiteral("B");
		builder5.addLiteral("F");
		Clause clause5 = builder5.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		conjunction.removeSubsumed(clause5);
		
		assertTrue(conjunction.contains(clause1));
		assertTrue(conjunction.contains(clause2));
		assertFalse(conjunction.contains(clause3));
		assertTrue(conjunction.contains(clause4));
	}
	
	@Test
	public void testRemoveSubsumed6() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		builder1.addLiteral("D");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("A");
		builder2.addLiteral("B");
		builder2.addLiteral("C");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("A");
		builder3.addLiteral("B");
		builder3.addLiteral("F");
		Clause clause3 = builder3.create();
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("A");
		builder4.addLiteral("G");
		Clause clause4 = builder4.create();
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addLiteral("A");
		Clause clause5 = builder5.create();

		ConjunctionOfClauses conjunction = new ConjunctionOfClausesImpl();
		conjunction.add(clause1);
		conjunction.addSubsuming(clause2);
		conjunction.addSubsuming(clause3);
		conjunction.addSubsuming(clause4);
		conjunction.removeSubsumed(clause5);
		
		assertFalse(conjunction.contains(clause1));
		assertFalse(conjunction.contains(clause2));
		assertFalse(conjunction.contains(clause3));
		assertFalse(conjunction.contains(clause4));
	}
	
}
