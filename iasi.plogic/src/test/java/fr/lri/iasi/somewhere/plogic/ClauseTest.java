package fr.lri.iasi.somewhere.plogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import java.util.Comparator;

import org.junit.Test;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;

public class ClauseTest {

	/**
	 * Basic equals test.
	 */
	@Test
	public void testEquals1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("l1");
		builder1.addLiteral("l2");
		builder1.addNegativeLiteral("l3");
		Clause clause1 = builder1.create();

		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("l1");
		builder2.addNegativeLiteral("l3");
		builder2.addLiteral("l2");
		Clause clause2 = builder2.create();

		assertEquals(clause2, clause1);
	}

	/**
	 * Empty clauses equals test.
	 */
	@Test
	public void testEquals2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		Clause clause1 = builder1.create();

		ClauseBuilder builder2 = new ClauseBuilder();
		Clause clause2 = builder2.create();

		assertEquals(clause2, clause1);
	}
	
	/**
	 * Unsorted clauses equals test.
	 */
	@Test
	public void testEquals3() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("l1");
		builder1.addLiteral("l2");
		builder1.addNegativeLiteral("l3");
		Clause clause1 = builder1.create();
		
		clause1.sort(new InverseComparator());

		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("l3");
		builder2.addLiteral("l1");
		builder2.addLiteral("l2");
		Clause clause2 = builder2.create();

		assertEquals(clause2, clause1);
	}
	
	/**
	 * Basic resolve test.
	 */
	@Test
	public void testResolve1() {
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("X");
		builder.addLiteral("B");
		Clause clause = builder.create();
		
		ClauseBuilder otherBuilder = new ClauseBuilder();
		otherBuilder.addNegativeLiteral("X");
		otherBuilder.addLiteral("Y");
		Clause otherClause = otherBuilder.create();
		
		ClauseBuilder resultBuilder = new ClauseBuilder();
		resultBuilder.addLiteral("B");
		resultBuilder.addLiteral("Y");
		Clause resultClause = resultBuilder.create();
		
		Clause compareClause = clause.resolve(otherClause, new LiteralImpl("X", true));
		assertEquals(compareClause, resultClause);		
	}
	
	/**
	 * Tautology resolve test.
	 */
	@Test
	public void testResolve2() {
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("A");
		builder.addLiteral("B");
		builder.addLiteral("C");
		builder.addLiteral("E");
		Clause clause = builder.create();
		
		ClauseBuilder otherBuilder = new ClauseBuilder();
		otherBuilder.addLiteral("D");
		otherBuilder.addNegativeLiteral("B");
		otherBuilder.addNegativeLiteral("C");
		Clause otherClause = otherBuilder.create();
		
		Clause resultClause = clause.resolve(otherClause, new LiteralImpl("B", true));
		assertTrue(resultClause.isTautology());		
	}
	
	/**
	 * Incoherent resolves must return null.
	 */
	@Test
	public void testResolve3() {
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("P");
		builder.addLiteral("Q");
		builder.addLiteral("D");
		Clause clause = builder.create();
		
		ClauseBuilder otherBuilder = new ClauseBuilder();
		otherBuilder.addLiteral("P");
		Clause otherClause = otherBuilder.create();
		
		Clause compareClause = clause.resolve(otherClause, new LiteralImpl("P", true));
		assertNull(compareClause);		
	}
	
	/**
	 * Unsatisfiable resolve test.
	 */
	@Test
	public void testResolve4() {
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("X");
		Clause clause = builder.create();
		
		ClauseBuilder otherBuilder = new ClauseBuilder();
		otherBuilder.addNegativeLiteral("X");
		Clause otherClause = otherBuilder.create();
		
		Clause resultClause = clause.resolve(otherClause, new LiteralImpl("X", true));
		assertTrue(!resultClause.isSat());
	}
	
	/**
	 *  Tautology resolve test.
	 */
	@Test
	public void testResolve5() {
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("X");
		builder.addNegativeLiteral("Z");
		Clause clause = builder.create();
		
		ClauseBuilder otherBuilder = new ClauseBuilder();
		otherBuilder.addNegativeLiteral("X");
		otherBuilder.addLiteral("Z");
		Clause otherClause = otherBuilder.create();
		
		Clause resultClause = clause.resolve(otherClause, new LiteralImpl("X", true));
		assertTrue(resultClause.isTautology());
		assertFalse(resultClause.contains(new LiteralImpl("X", true)));
		assertFalse(resultClause.contains(new LiteralImpl("X", false)));
		assertFalse(resultClause.contains(new LiteralImpl("Z", true)));
		assertFalse(resultClause.contains(new LiteralImpl("Z", false)));
	}
	
	/**
	 *  Basic subsumption test.
	 */
	@Test
	public void testSumbsumption1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("X");
		builder1.addLiteral("Y");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("X");
		Clause clause2 = builder2.create();
		
		assertFalse(clause2.subsumes(clause1));
	}
	
	/**
	 *  Equals clauses subsumes each other.
	 */
	@Test
	public void testSumbsumption2() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("X");
		builder1.addLiteral("Y");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("X");
		builder2.addLiteral("Y");
		Clause clause2 = builder2.create();
		
		assertTrue(clause2.subsumes(clause1));
		assertTrue(clause1.subsumes(clause2));
	}
	
	/**
	 *  Unsorted clauses subsumption test.
	 */
	@Test
	public void testSumbsumption3() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("X");
		builder1.addLiteral("Y");
		builder1.addLiteral("Z");
		builder1.addNegativeLiteral("K");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("K");
		builder2.addLiteral("Y");
		Clause clause2 = builder2.create();
		
		assertTrue(clause2.subsumes(clause1));
	}
	
	/**
	 *  Empty clause subsumption test.
	 */
	@Test
	public void testEmptySumbsumption() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("X");
		builder1.addLiteral("Y");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		Clause clause2 = builder2.create();
		
		assertTrue(clause2.subsumes(clause1));
	}
	
	/**
	 *  Basic union with common elements test.
	 */
	@Test
	public void testUnion1() {
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		builder1.addLiteral("B");
		builder1.addLiteral("C");
		Clause clause1 = builder1.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("C");
		builder2.addLiteral("D");
		builder2.addLiteral("E");
		Clause clause2 = builder2.create();
		
		ClauseBuilder resultBuilder = new ClauseBuilder();
		resultBuilder.addLiteral("A");
		resultBuilder.addLiteral("B");
		resultBuilder.addLiteral("C");
		resultBuilder.addLiteral("D");
		resultBuilder.addLiteral("E");
		Clause resultClause = resultBuilder.create();
		
		Clause compareClause = clause1.union(clause2);
		assertEquals(compareClause, resultClause);
	}
	
	/**
	 *  Tautology union test.
	 */
	@Test
	public void testUnion2() {
		ClauseBuilder builder = new ClauseBuilder();
		builder.addLiteral("X");
		Clause clause1 = builder.create();
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("X");
		Clause clause2 = builder2.create();
		
		ClauseBuilder builder3  = new ClauseBuilder();
		builder3.addLiteral("X");
		Clause clause3 = builder3.create();
		
		Clause resultClause = clause1.union(clause2);
		assertTrue(resultClause.isTautology());
		
		resultClause = resultClause.union(clause3);
		assertTrue(resultClause.isTautology());
		assertFalse(resultClause.contains(new LiteralImpl("X", true)));
		assertFalse(resultClause.contains(new LiteralImpl("X", false)));
	}
	
	/**
	 * Literal comparator sorting by inverse natural order.
	 * 
	 * NOTE : For tests only!
	 * @author Andre Fonseca
	 *
	 */
	private static class InverseComparator implements Comparator<Literal> {

		@Override
		public int compare(Literal l1, Literal l2) {
			int result = - (l1.getLabel().compareToIgnoreCase(l2.getLabel()));
			
			if (result == 0) {
				if (!l1.getSign() && l2.getSign()) return -1;
				if (l1.getSign() && !l2.getSign()) return 1;
			}
			 		
			return result;
		}
		
	}

}
