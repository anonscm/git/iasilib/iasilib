package fr.lri.iasi.somewhere.plogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

import org.junit.Test;

import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;

public class LiteralTest {
	
	/**
	 * Basic equals test
	 */
	@Test
	public void testEquals1() {
		Literal lit1 = new LiteralImpl("1", true);
		Literal lit2 = new LiteralImpl("1", true);

		assertEquals(lit2, lit1);
	}
	
	/**
	 * Basic equals test : different signals
	 */ 
	@Test
	public void testEquals2() {
		Literal lit1 = new LiteralImpl("1", true);
		Literal lit2 = new LiteralImpl("1", false);

		assertThat(lit1, is(not(lit2)));
	}

}
