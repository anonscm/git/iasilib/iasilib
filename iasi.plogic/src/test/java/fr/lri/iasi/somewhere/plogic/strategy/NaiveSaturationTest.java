package fr.lri.iasi.somewhere.plogic.strategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;

public class NaiveSaturationTest {
	
	@Test
	public void test1() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("p");
		builder2.addLiteral("p1");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("p");
		builder3.addLiteral("p2");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("r");
		builder4.addLiteral("r1");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("r");
		builder5.addLiteral("r2");
		theory.add(builder5.create());

		// Calling the naive saturation
		ConjunctionOfClauses result = theory.initialize();
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl(theory);
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("q");
		builder7.addLiteral("p1");
		builder7.addLiteral("r1");
		expectedRes.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("q");
		builder8.addLiteral("p1");
		builder8.addLiteral("r");
		expectedRes.add(builder8.create());
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addNegativeLiteral("q");
		builder9.addLiteral("p1");
		builder9.addLiteral("r2");
		expectedRes.add(builder9.create());
		
		ClauseBuilder builder10 = new ClauseBuilder();
		builder10.addNegativeLiteral("q");
		builder10.addLiteral("p");
		builder10.addLiteral("r1");
		expectedRes.add(builder10.create());
		
		ClauseBuilder builder12 = new ClauseBuilder();
		builder12.addNegativeLiteral("q");
		builder12.addLiteral("p");
		builder12.addLiteral("r2");
		expectedRes.add(builder12.create());
		
		ClauseBuilder builder13 = new ClauseBuilder();
		builder13.addNegativeLiteral("q");
		builder13.addLiteral("p2");
		builder13.addLiteral("r1");
		expectedRes.add(builder13.create());
		
		ClauseBuilder builder14 = new ClauseBuilder();
		builder14.addNegativeLiteral("q");
		builder14.addLiteral("p2");
		builder14.addLiteral("r");
		expectedRes.add(builder14.create());
		
		ClauseBuilder builder15 = new ClauseBuilder();
		builder15.addNegativeLiteral("q");
		builder15.addLiteral("p2");
		builder15.addLiteral("r2");
		expectedRes.add(builder15.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	
	@Test
	public void test2() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();

		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("A");
		builder2.addLiteral("B");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("C");
		builder3.addLiteral("B");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("D");
		builder4.addNegativeLiteral("B");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("B");
		builder5.addLiteral("F");
		theory.add(builder5.create());
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("E");
		builder7.addLiteral("C");
		builder7.addLiteral("D");
		theory.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("F");
		builder8.addLiteral("G");
		theory.add(builder8.create());

		// Calling the naive saturation
		ConjunctionOfClauses result = theory.initialize();
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addLiteral("A");
		expectedRes.add(builder9.create());
		
		ClauseBuilder builder10 = new ClauseBuilder();
		builder10.addLiteral("B");
		expectedRes.add(builder10.create());
		
		ClauseBuilder builder11 = new ClauseBuilder();
		builder11.addNegativeLiteral("D");
		expectedRes.add(builder11.create());
		
		ClauseBuilder builder12 = new ClauseBuilder();
		builder12.addLiteral("F");
		expectedRes.add(builder12.create());
		
		ClauseBuilder builder13 = new ClauseBuilder();
		builder13.addLiteral("G");
		expectedRes.add(builder13.create());
		
		ClauseBuilder builder14 = new ClauseBuilder();
		builder14.addNegativeLiteral("E");
		builder14.addLiteral("C");
		expectedRes.add(builder14.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
}
