package fr.lri.iasi.somewhere.plogic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.LiteralImpl;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;

@RunWith(Parameterized.class)
public class PropositionalTheoryTest {
	
	/* --- PARAMETERS SETTINGS --- */ 
	public PropositionalTheoryTest(String dataType) {
		StrategiesConfigurator.getInstance().setDataType(dataType);
	}
	
	@Parameterized.Parameters
	public static List<Object[]> parameters() {
		return Arrays.asList(new Object[][] {
			      {"hashSet"},
			      {"trie"}
			    });
	}
	
	/* --- Tests --- */
	/**
	 * Verify if literalIndex is updated after the 'removeSubsumed' operation.
	 */
	@Test
	public void test1() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		theory.add(builder1.create());
		
		// Removing clauses subsumed by the clause (!q) => Every clause!
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("q");
		theory.removeSubsumed(builder2.create());
		
		// Verifying the literalIndex: must have nothing inside!
		assertTrue(theory.getVocabulary().isEmpty());
	}
	
	/**
	 * Verify if literalIndex is updated after the 'addSubsuming' operation.
	 */
	@Test
	public void test2() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		theory.add(builder1.create());
		
		// Adding subsuming the clause (!q) => Remove Every clause!
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("q");
		Clause clause2 = builder2.create();
		theory.addSubsuming(clause2);
		
		// Verifying the literalIndex: must have nothing inside!
		Collection<Clause> content = new LinkedHashSet<Clause>();
		content.add(clause2);
		assertThat(theory.getVocabularyFor(new LiteralImpl("q", false)), is(content));
	}
	
	/**
	 * Verify if literalIndex is updated after the 'add' operation.
	 */
	@Test
	public void test3() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		Clause clause1 = builder1.create();
		theory.add(clause1);
		
		// Verifying the literalIndex. Each literal must refers to the same clause!
		Collection<Clause> content = new LinkedHashSet<Clause>();
		content.add(clause1);
		assertThat(theory.getVocabularyFor(new LiteralImpl("q", false)), is(content));
		assertThat(theory.getVocabularyFor(new LiteralImpl("p", true)), is(content));
		assertThat(theory.getVocabularyFor(new LiteralImpl("r", true)), is(content));
	}
	
	/**
	 * Verify if literalIndex is updated after the 'remove' operation.
	 */
	@Test
	public void test4() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		Clause clause1 = builder1.create();
		theory.add(clause1);
		
		// Removing clause 1
		theory.remove(clause1);
		
		// Verifying the literalIndex. Each literal must refers to a empty list!
		Collection<Clause> content = new LinkedHashSet<Clause>();
		assertThat(theory.getVocabularyFor(new LiteralImpl("q", false)), is(content));
		assertThat(theory.getVocabularyFor(new LiteralImpl("p", true)), is(content));
		assertThat(theory.getVocabularyFor(new LiteralImpl("r", true)), is(content));
	}
	
	/**
	 * Verify if canonical map is updated after the 'add' operation.
	 */
	@Test
	public void test5() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		Clause clause1 = builder1.create();
		theory.add(clause1);
		
		Clause canonicalClause = clause1.canonicalCopy();
		
		// Verifying the literalIndex. Each literal must refers to a empty list!
		assertThat(theory.getCanonicalMap().get(canonicalClause), is(clause1));
	}
	
	/**
	 * Verify if canonical map is updated after the 'remove' operation.
	 */
	@Test
	public void test6() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		// Adding clause (!q v p v r) 
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		Clause clause1 = builder1.create();
		theory.add(clause1);
		
		Clause canonicalClause = clause1.canonicalCopy();
		
		// Removing clause 1
		theory.remove(clause1);
		
		// Verifying the literalIndex. Each literal must refers to a empty list!
		assertThat(theory.getCanonicalMap().get(canonicalClause), is(nullValue()));
	}

}
