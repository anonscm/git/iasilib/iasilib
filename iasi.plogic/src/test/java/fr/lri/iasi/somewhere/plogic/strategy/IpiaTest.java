package fr.lri.iasi.somewhere.plogic.strategy;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseBuilder;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesImpl;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;

@RunWith(Parameterized.class)
public class IpiaTest {
	
	/* --- PARAMETERS SETTINGS --- */ 
	public IpiaTest(String dataType) {
		StrategiesConfigurator.getInstance().setDataType(dataType);
	}
	
	@Parameterized.Parameters
	public static List<Object[]> parameters() {
		return Arrays.asList(new Object[][] {
			      {"hashSet"},
			      {"trie"}
			    });
	}
	
	/* --- Tests --- */
	@Test
	public void test1() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		builder1.addLiteral("p");
		builder1.addLiteral("r");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("p");
		builder2.addLiteral("p1");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();

		builder3.addNegativeLiteral("p");
		builder3.addLiteral("p2");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("r");
		builder4.addLiteral("r1");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("r");
		builder5.addLiteral("r2");
		theory.add(builder5.create());
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("q");
		builder7.addLiteral("p1");
		builder7.addLiteral("r1");
		theory.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("q");
		builder8.addLiteral("p1");
		builder8.addLiteral("r");
		theory.add(builder8.create());
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addNegativeLiteral("q");
		builder9.addLiteral("p1");
		builder9.addLiteral("r2");
		theory.add(builder9.create());
		
		ClauseBuilder builder10 = new ClauseBuilder();
		builder10.addNegativeLiteral("q");
		builder10.addLiteral("p");
		builder10.addLiteral("r1");
		theory.add(builder10.create());
		
		ClauseBuilder builder12 = new ClauseBuilder();
		builder12.addNegativeLiteral("q");
		builder12.addLiteral("p");
		builder12.addLiteral("r2");
		theory.add(builder12.create());
		
		ClauseBuilder builder13 = new ClauseBuilder();
		builder13.addNegativeLiteral("q");
		builder13.addLiteral("p2");
		builder13.addLiteral("r1");
		theory.add(builder13.create());
		
		ClauseBuilder builder14 = new ClauseBuilder();
		builder14.addNegativeLiteral("q");
		builder14.addLiteral("p2");
		builder14.addLiteral("r");
		theory.add(builder14.create());
		
		ClauseBuilder builder15 = new ClauseBuilder();
		builder15.addNegativeLiteral("q");
		builder15.addLiteral("p2");
		builder15.addLiteral("r2");
		theory.add(builder15.create());
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addLiteral("q");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		// Calling ipia
		ConjunctionOfClauses result = theory.produceConsequentsFrom(clausesToAdd, false);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder16 = new ClauseBuilder();
		builder16.addLiteral("q");
		expectedRes.add(builder16.create());
		
		ClauseBuilder builder17 = new ClauseBuilder();
		builder17.addLiteral("p1");
		builder17.addLiteral("r1");
		expectedRes.add(builder17.create());
		
		ClauseBuilder builder18 = new ClauseBuilder();
		builder18.addLiteral("p1");
		builder18.addLiteral("r");
		expectedRes.add(builder18.create());
		
		ClauseBuilder builder19 = new ClauseBuilder();
		builder19.addLiteral("p1");
		builder19.addLiteral("r2");
		expectedRes.add(builder19.create());
		
		ClauseBuilder builder20 = new ClauseBuilder();
		builder20.addLiteral("p");
		builder20.addLiteral("r1");
		expectedRes.add(builder20.create());
		
		ClauseBuilder builder21 = new ClauseBuilder();
		builder21.addLiteral("p");
		builder21.addLiteral("r");
		expectedRes.add(builder21.create());
		
		ClauseBuilder builder22 = new ClauseBuilder();
		builder22.addLiteral("p");
		builder22.addLiteral("r2");
		expectedRes.add(builder22.create());
		
		ClauseBuilder builder23 = new ClauseBuilder();
		builder23.addLiteral("p2");
		builder23.addLiteral("r1");
		expectedRes.add(builder23.create());
		
		ClauseBuilder builder24 = new ClauseBuilder();
		builder24.addLiteral("p2");
		builder24.addLiteral("r");
		expectedRes.add(builder24.create());
		
		ClauseBuilder builder25 = new ClauseBuilder();
		builder25.addLiteral("p2");
		builder25.addLiteral("r2");
		expectedRes.add(builder25.create());
		
		ClauseBuilder builder26 = new ClauseBuilder();
		builder26.addNegativeLiteral("p");
		builder26.addLiteral("p1");
		expectedRes.add(builder26.create());
		
		ClauseBuilder builder27 = new ClauseBuilder();
		builder27.addNegativeLiteral("p");
		builder27.addLiteral("p2");
		expectedRes.add(builder27.create());
		
		ClauseBuilder builder28 = new ClauseBuilder();
		builder28.addNegativeLiteral("r");
		builder28.addLiteral("r1");
		expectedRes.add(builder28.create());
		
		ClauseBuilder builder29 = new ClauseBuilder();
		builder29.addNegativeLiteral("r");
		builder29.addLiteral("r2");
		expectedRes.add(builder29.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void test2() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("p");
		builder1.addLiteral("q");
		theory.add(builder1.create());
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addNegativeLiteral("q");
		builderQuery.addLiteral("p");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		// Calling ipia
		ConjunctionOfClauses result = theory.produceConsequentsFrom(clausesToAdd, false);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		expectedRes.add(builder1.create());
		expectedRes.add(builderQuery.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void test3() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("q");
		theory.add(builder1.create());
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addLiteral("q");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		// Calling ipia
		ConjunctionOfClauses result = theory.produceConsequentsFrom(clausesToAdd, true);
		
		// Build expected set
		ClauseBuilder builderEmpty = new ClauseBuilder();
		builderEmpty.addLiteral("empty");
		
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		expectedRes.add(builderEmpty.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void test4() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("t");
		builder1.addLiteral("x");
		builder1.addNegativeLiteral("y");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("t");
		builder2.addNegativeLiteral("y");
		builder2.addNegativeLiteral("z");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("t");
		builder3.addNegativeLiteral("x");
		builder3.addNegativeLiteral("z");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("a");
		builder4.addNegativeLiteral("b");
		builder4.addLiteral("c");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("a");
		builder5.addLiteral("b");
		builder5.addNegativeLiteral("c");
		theory.add(builder5.create());
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addLiteral("a");
		builderQuery.addNegativeLiteral("c");
		builderQuery.addLiteral("t");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		// Calling ipia
		ConjunctionOfClauses result = theory.produceConsequentsFrom(clausesToAdd, false);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl(theory);
		expectedRes.add(builderQuery.create());
		
		ClauseBuilder builder6 = new ClauseBuilder();
		builder6.addLiteral("b");
		builder6.addNegativeLiteral("c");
		builder6.addLiteral("t");
		expectedRes.add(builder6.create());
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("t");
		builder7.addLiteral("x");
		builder7.addNegativeLiteral("y");
		expectedRes.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("t");
		builder8.addNegativeLiteral("y");
		builder8.addNegativeLiteral("z");
		expectedRes.add(builder8.create());
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addNegativeLiteral("t");
		builder9.addNegativeLiteral("x");
		builder9.addNegativeLiteral("z");
		expectedRes.add(builder9.create());
		
		ClauseBuilder builder10 = new ClauseBuilder();
		builder10.addLiteral("b");
		builder10.addNegativeLiteral("c");
		builder10.addLiteral("x");
		builder10.addNegativeLiteral("y");
		expectedRes.add(builder10.create());
		
		ClauseBuilder builder11 = new ClauseBuilder();
		builder11.addLiteral("b");
		builder11.addNegativeLiteral("c");
		builder11.addNegativeLiteral("y");
		builder11.addNegativeLiteral("z");
		expectedRes.add(builder11.create());
		
		ClauseBuilder builder12 = new ClauseBuilder();
		builder12.addLiteral("b");
		builder12.addNegativeLiteral("c");
		builder12.addNegativeLiteral("x");
		builder12.addNegativeLiteral("z");
		expectedRes.add(builder12.create());
		
		ClauseBuilder builder13 = new ClauseBuilder();
		builder13.addLiteral("a");
		builder13.addNegativeLiteral("b");
		builder13.addLiteral("t");
		expectedRes.add(builder13.create());
		
		ClauseBuilder builder14 = new ClauseBuilder();
		builder14.addLiteral("a");
		builder14.addNegativeLiteral("c");
		builder14.addLiteral("x");
		builder14.addNegativeLiteral("y");
		expectedRes.add(builder14.create());
		
		ClauseBuilder builder15 = new ClauseBuilder();
		builder15.addLiteral("a");
		builder15.addNegativeLiteral("c");
		builder15.addNegativeLiteral("y");
		builder15.addNegativeLiteral("z");
		expectedRes.add(builder15.create());
		
		ClauseBuilder builder16 = new ClauseBuilder();
		builder16.addLiteral("a");
		builder16.addNegativeLiteral("c");
		builder16.addNegativeLiteral("x");
		builder16.addNegativeLiteral("z");
		expectedRes.add(builder16.create());
		
		ClauseBuilder builder17 = new ClauseBuilder();
		builder17.addNegativeLiteral("t");
		builder17.addLiteral("x");
		builder17.addNegativeLiteral("y");
		expectedRes.add(builder17.create());
		
		ClauseBuilder builder18 = new ClauseBuilder();
		builder18.addNegativeLiteral("t");
		builder18.addNegativeLiteral("y");
		builder18.addNegativeLiteral("z");
		expectedRes.add(builder18.create());
		
		ClauseBuilder builder19 = new ClauseBuilder();
		builder19.addNegativeLiteral("t");
		builder19.addNegativeLiteral("x");
		builder19.addNegativeLiteral("z");
		expectedRes.add(builder19.create());
		
		ClauseBuilder builder20 = new ClauseBuilder();
		builder20.addLiteral("a");
		builder20.addNegativeLiteral("b");
		builder20.addLiteral("x");
		builder20.addNegativeLiteral("y");
		expectedRes.add(builder20.create());
		
		ClauseBuilder builder21 = new ClauseBuilder();
		builder21.addLiteral("a");
		builder21.addNegativeLiteral("b");
		builder21.addNegativeLiteral("y");
		builder21.addNegativeLiteral("z");
		expectedRes.add(builder21.create());
		
		ClauseBuilder builder22 = new ClauseBuilder();
		builder22.addLiteral("a");
		builder22.addNegativeLiteral("b");
		builder22.addNegativeLiteral("x");
		builder22.addNegativeLiteral("z");
		expectedRes.add(builder22.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void test5() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("b");
		builder1.addNegativeLiteral("d");
		builder1.addNegativeLiteral("f");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addLiteral("b");
		builder2.addNegativeLiteral("d");
		builder2.addLiteral("f");
		theory.add(builder2.create());
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addLiteral("b");
		builderQuery.addLiteral("e");
		builderQuery.addNegativeLiteral("d");
		builderQuery.addNegativeLiteral("f");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		// Calling ipia
		ConjunctionOfClauses result = theory.produceConsequentsFrom(clausesToAdd, true);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addLiteral("e");
		builder3.addNegativeLiteral("d");
		builder3.addNegativeLiteral("f");
		expectedRes.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addLiteral("e");
		builder4.addNegativeLiteral("d");
		builder4.addLiteral("b");
		expectedRes.add(builder4.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void test6() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("p");
		builder1.addLiteral("q");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("j");
		builder2.addLiteral("p");
		theory.add(builder2.create());
		
		// Calling Ipia
		ConjunctionOfClauses saturatedClauseSet = theory.initialize();
		PropositionalClausalTheory saturatedTheory = new PropositionalClausalTheoryImpl(saturatedClauseSet);
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addNegativeLiteral("q");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		ConjunctionOfClauses result = saturatedTheory.produceConsequentsFrom(clausesToAdd, false);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("p");
		expectedRes.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("q");
		expectedRes.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("j");
		expectedRes.add(builder5.create());

		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void test7() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("p");
		builder1.addLiteral("q");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("f");
		builder2.addLiteral("p");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("f");
		builder3.addLiteral("q");
		theory.add(builder3.create());
		
		// Query
		ClauseBuilder builderQuery = new ClauseBuilder();
		builderQuery.addNegativeLiteral("q");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builderQuery.create());
		
		// Calling ipia
		ConjunctionOfClauses result = theory.produceConsequentsFrom(clausesToAdd, true);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("p");
		expectedRes.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("q");
		expectedRes.add(builder5.create());
		
		ClauseBuilder builder6 = new ClauseBuilder();
		builder6.addNegativeLiteral("f");
		expectedRes.add(builder6.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	@Test
	public void testTrip() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("Far");
		builder1.addLiteral("Expensive");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("Far");
		builder2.addLiteral("Int");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("Far");
		builder3.addLiteral("Chile");
		builder3.addLiteral("Kenya");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("Int");
		builder4.addLiteral("Pass");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("Kenya");
		builder5.addLiteral("YellowFever");
		theory.add(builder5.create());
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("Lodge");
		builder7.addNegativeLiteral("Kenya");
		builder7.addLiteral("Paludisme");
		theory.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("Kenya");
		builder8.addLiteral("Lodge");
		theory.add(builder8.create());
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addNegativeLiteral("Chile");
		builder9.addLiteral("Hotel");
		theory.add(builder9.create());
		
		// Calling ipia
		PropositionalClausalTheory initalTheory = new PropositionalClausalTheoryImpl();
		ConjunctionOfClauses result = initalTheory.produceConsequentsFrom(theory, false);
		
		// Remove expected subsumed clause
		theory.remove(builder7.create());
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl(theory);
		
		ClauseBuilder builder17 = new ClauseBuilder();
		builder17.addNegativeLiteral("Far");
		builder17.addLiteral("Chile");
		builder17.addLiteral("YellowFever");
		expectedRes.add(builder17.create());
		
		ClauseBuilder builder18 = new ClauseBuilder();
		builder18.addNegativeLiteral("Far");
		builder18.addLiteral("Chile");
		builder18.addLiteral("Lodge");
		expectedRes.add(builder18.create());
		
		ClauseBuilder builder19 = new ClauseBuilder();
		builder19.addNegativeLiteral("Far");
		builder19.addLiteral("Hotel");
		builder19.addLiteral("YellowFever");
		expectedRes.add(builder19.create());
		
		ClauseBuilder builder20 = new ClauseBuilder();
		builder20.addNegativeLiteral("Far");
		builder20.addLiteral("Hotel");
		builder20.addLiteral("Lodge");
		expectedRes.add(builder20.create());
		
		ClauseBuilder builder21 = new ClauseBuilder();
		builder21.addNegativeLiteral("Far");
		builder21.addLiteral("Pass");
		expectedRes.add(builder21.create());
		
		ClauseBuilder builder22 = new ClauseBuilder();
		builder22.addNegativeLiteral("Kenya");
		builder22.addLiteral("Paludisme");
		expectedRes.add(builder22.create());
		
		ClauseBuilder builder23 = new ClauseBuilder();
		builder23.addLiteral("Paludisme");
		builder23.addNegativeLiteral("Far");
		builder23.addLiteral("Chile");
		expectedRes.add(builder23.create());
		
		ClauseBuilder builder24 = new ClauseBuilder();
		builder24.addLiteral("Paludisme");
		builder24.addNegativeLiteral("Far");
		builder24.addLiteral("Hotel");
		expectedRes.add(builder24.create());
		
		ClauseBuilder builder25 = new ClauseBuilder();
		builder25.addNegativeLiteral("Far");
		builder25.addLiteral("Kenya");
		builder25.addLiteral("Hotel");
		expectedRes.add(builder25.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	
	@Test
	public void testIBug4() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addNegativeLiteral("A");
		builder1.addLiteral("B");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("C");
		builder2.addLiteral("A");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("D");
		builder3.addLiteral("C");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("F");
		builder4.addLiteral("E");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("Z");
		builder5.addLiteral("C");
		theory.add(builder5.create());
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addLiteral("Y");
		builder7.addNegativeLiteral("E");
		theory.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("Y");
		builder8.addLiteral("Z");
		theory.add(builder8.create());
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addNegativeLiteral("W");
		builder9.addLiteral("Y");
		theory.add(builder9.create());
		
		ClauseBuilder builder10 = new ClauseBuilder();
		builder10.addNegativeLiteral("B");
		theory.add(builder10.create());
		
		// Calling ipia
		PropositionalClausalTheory initalTheory = new PropositionalClausalTheoryImpl();
		ConjunctionOfClauses result = initalTheory.produceConsequentsFrom(theory, true);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder16 = new ClauseBuilder();
		builder16.addNegativeLiteral("B");
		expectedRes.add(builder16.create());
		
		ClauseBuilder builder17 = new ClauseBuilder();
		builder17.addNegativeLiteral("A");
		expectedRes.add(builder17.create());
		
		ClauseBuilder builder18 = new ClauseBuilder();
		builder18.addNegativeLiteral("C");
		expectedRes.add(builder18.create());
		
		ClauseBuilder builder19 = new ClauseBuilder();
		builder19.addNegativeLiteral("D");
		expectedRes.add(builder19.create());
		
		ClauseBuilder builder20 = new ClauseBuilder();
		builder20.addNegativeLiteral("Z");
		expectedRes.add(builder20.create());
		
		ClauseBuilder builder21 = new ClauseBuilder();
		builder21.addNegativeLiteral("Y");
		expectedRes.add(builder21.create());
		
		ClauseBuilder builder22 = new ClauseBuilder();
		builder22.addNegativeLiteral("W");
		expectedRes.add(builder22.create());
		
		ClauseBuilder builder23 = new ClauseBuilder();
		builder23.addNegativeLiteral("E");
		expectedRes.add(builder23.create());
		
		ClauseBuilder builder24 = new ClauseBuilder();
		builder24.addNegativeLiteral("F");
		expectedRes.add(builder24.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}
	
	
	@Test
	public void testIBug7() {
		// Building theory
		PropositionalClausalTheory theory = new PropositionalClausalTheoryImpl();

		ClauseBuilder builder1 = new ClauseBuilder();
		builder1.addLiteral("A");
		theory.add(builder1.create());
		
		ClauseBuilder builder2 = new ClauseBuilder();
		builder2.addNegativeLiteral("A");
		builder2.addLiteral("B");
		theory.add(builder2.create());
		
		ClauseBuilder builder3 = new ClauseBuilder();
		builder3.addNegativeLiteral("C");
		builder3.addLiteral("B");
		theory.add(builder3.create());
		
		ClauseBuilder builder4 = new ClauseBuilder();
		builder4.addNegativeLiteral("D");
		builder4.addNegativeLiteral("B");
		theory.add(builder4.create());
		
		ClauseBuilder builder5 = new ClauseBuilder();
		builder5.addNegativeLiteral("B");
		builder5.addLiteral("F");
		theory.add(builder5.create());
		
		ClauseBuilder builder7 = new ClauseBuilder();
		builder7.addNegativeLiteral("E");
		builder7.addLiteral("C");
		builder7.addLiteral("D");
		theory.add(builder7.create());
		
		ClauseBuilder builder8 = new ClauseBuilder();
		builder8.addNegativeLiteral("F");
		builder8.addLiteral("G");
		theory.add(builder8.create());

		// Calling Ipia
		ConjunctionOfClauses saturatedClauseSet = theory.initialize();
		PropositionalClausalTheory saturatedTheory = new PropositionalClausalTheoryImpl(saturatedClauseSet);
		
		ClauseBuilder builder9 = new ClauseBuilder();
		builder9.addNegativeLiteral("E");
		ConjunctionOfClauses clausesToAdd = new ConjunctionOfClausesImpl();
		clausesToAdd.add(builder9.create());
		
		ConjunctionOfClauses result = saturatedTheory.produceConsequentsFrom(clausesToAdd, false);
		
		// Build expected set
		PropositionalClausalTheory expectedRes = new PropositionalClausalTheoryImpl();
		
		ClauseBuilder builder16 = new ClauseBuilder();
		builder16.addLiteral("A");
		expectedRes.add(builder16.create());
		
		ClauseBuilder builder17 = new ClauseBuilder();
		builder17.addLiteral("B");
		expectedRes.add(builder17.create());
		
		ClauseBuilder builder18 = new ClauseBuilder();
		builder18.addNegativeLiteral("D");
		expectedRes.add(builder18.create());
		
		ClauseBuilder builder19 = new ClauseBuilder();
		builder19.addLiteral("F");
		expectedRes.add(builder19.create());
		
		ClauseBuilder builder20 = new ClauseBuilder();
		builder20.addLiteral("G");
		expectedRes.add(builder20.create());
		
		ClauseBuilder builder21 = new ClauseBuilder();
		builder21.addNegativeLiteral("E");
		expectedRes.add(builder21.create());
		
		// Comparing result
		assertEquals(expectedRes, result);
	}

}
