package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

/**
 * Describes a clause subsumption operation on a propositional theory, that
 * uses the theorie's clause index in order to reduce the quantity of
 * subsumption verifications.
 * 
 * @author Andre Fonseca
 *
 */
public class TheoryIndexSubsumesStrategy implements SubsumesStrategy<PropositionalClausalTheory, Clause> {

	/**
     * {@inheritDoc}
     * 
     * NOTE : This operation, normally used from the delegate object,
     * was overwritten in the theory in order to profit from the 'literalIndex'
     * achieving a better performance.
     */
	@Override
	public boolean performSubsumes(PropositionalClausalTheory formula,
			Clause otherFormula) {
		
		if (otherFormula.isTautology())
			return true;
		
		for (Literal lit : otherFormula) {
			Collection<Clause> relatedClauses = formula.getVocabulary().get(lit);
			if (relatedClauses == null)
				continue;
			
			for (Clause clause : relatedClauses) {
				if (clause.subsumes(otherFormula))
					return true;
			}
		}
		
		return false;
	}

}
