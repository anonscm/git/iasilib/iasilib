package fr.lri.iasi.libs.plogic;

import java.util.Set;

import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;

/**
 * Strategy that describes the addition of a formula on container formula.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the current formula (container)
 * @param <F> The type of the formula to be added (containing)
 */
public interface AddFormulaStrategy<T extends Set<F>, F> {
	
	/**
	 * Add a formula on the container formula
	 * 
	 * @param collection : is the container formula
	 * @param formula : is the formula to be added
	 * @return true if the formula was correctly added, false otherwise
	 */
	boolean performAdd(T collection, F formula);
	
	/**
	 * Add a formula on the container formula removing other formulas
	 * that are subsumed by it.
	 * 
	 * @param collection : is the container formula
	 * @param clause : is the formula to be added
	 * @return a result pair having as first element a boolean result indicating if the 
	 * formula was correctly added, and a collection as second element, indicating the 
	 * subsumed formulas, if any.
	 */
	ResultPair performAddSubsuming(T collection, F clause);

}
