package fr.lri.iasi.libs.plogic;


import fr.lri.iasi.libs.formallogic.Indexable;

/**
 * The propositional theory representation.
 * 
 * @author Andre Fonseca
 *
 */
public interface PropositionalClausalTheory extends ConjunctionOfClauses, Indexable<PropositionalClausalTheory, Literal, Clause> {
	
	/**
	 * Produce new consequents from the addition of a clause (or collection of clauses) to an initial theory.
	 * It delegates the consequence finding operation to the responsible strategy.
	 * 
	 * @param clausesToAdd : the collection of clauses to be added
	 * @param onlyNewConsequents : indicates if the result collection of clauses will subtract the initial theory, returning only
	 * the new produced consequences.
	 * @return the set of clauses result of the consequence finding operation
	 */
	ConjunctionOfClauses produceConsequentsFrom(ConjunctionOfClauses clausesToAdd, boolean onlyNewConsequents);
	
	/**
	 * Produce new consequents from the addition of a clause (or collection of clauses) to an initial theory.
	 * This method assumes that "alreadyAdded" was already added to this theory.
	 * It delegates the consequence finding operation to the responsible strategy.
	 * 
	 * @param clausesToAdd : the collection of clauses to be added
	 * @param alreadyAdded : clauses already added to this theory, if any
	 * @param onlyNewConsequents : indicates if the result collection of clauses will subtract the initial theory, returning only
	 * the new produced consequences.
	 * @return the set of clauses result of the consequence finding operation
	 */
	ConjunctionOfClauses produceConsequentsFrom(ConjunctionOfClauses clausesToAdd, ConjunctionOfClauses alreadyAdded, boolean onlyNewConsequents);
	
	/**
	 * Initialize the theory. Normally, it delegates the initialize operation to the responsible strategy. Examples
	 * of initialize
	 * 
	 * @return the set of clauses result of the initialize operation
	 */
	ConjunctionOfClauses initialize();
	
}
