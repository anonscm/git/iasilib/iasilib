package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;
import fr.lri.iasi.libs.plogic.structures.ClauseTrie;
import fr.lri.iasi.libs.plogic.structures.ClauseTrieNode;

/**
 * Clause removal operation on a clause conjunction (trie).
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseTrieRemoveFormulaStrategy implements RemoveFormulaStrategy<ConjunctionOfClauses, Clause> {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performRemove(ConjunctionOfClauses collection, Object formula) {
		return collection.delegate().remove(formula);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Clause> performRemoveSubsumed(ConjunctionOfClauses collection,
			Clause formula) {
		// List of clauses to be deleted at the end of this operation
		Set<Clause> result = new LinkedHashSet<Clause>();
		
		if (!formula.isSat()) {
			collection.clear();
			return result;
		}
		
		if (formula.isTautology())
			return result;
		
		Comparator<Literal> comparator = ((ClauseTrie)collection.delegate()).getComparator();
		formula.sort(comparator);
		
		// If some node was marked to be removed, return the 'nodesToDelete' list.
		ClauseTrieNode root = ((ClauseTrie)collection.delegate()).getRoot();
		if (removeSubsumedHelper(formula, root, result, comparator)) {
			
			// These clauses must be deleted at the end of the operation because the trie cannot be rebuild
			// during the subsumption verification
			collection.removeAll(result);
		}
		
		return result;
	}
			
	/**
	 * Helper function to compute subsequent steps of the remove subsumed operation.
	 * @param remaingLiterals of the verified clause
	 * @param node the current node on the recursion
	 * @param clausesToDelete list of clauses to be deleted at the end of the operation.
	 */
	private boolean removeSubsumedHelper(Clause remaingLiterals, ClauseTrieNode node, Set<Clause> nodesToDelete, Comparator<Literal> comparator) {
		if (remaingLiterals.isEmpty())
			return true;
		
		if (node.isLeaf())
			return false;
		
		// Taking first element of the clause
		Literal first = Iterables.getFirst(remaingLiterals, null);
		
		// Assures the order of the children list
		List<ClauseTrieNode> children = node.getChildren();
		if (!Ordering.natural().isOrdered(children))
			Collections.sort(children);
		
		Iterator<ClauseTrieNode> it = children.iterator();
		while (it.hasNext()) {
			ClauseTrieNode child = it.next();
	
			if (child.getLabel().equals(first)) {
				
				// Must avoid concurrent modifications removing the child using the Iterator
				Clause remain = remaingLiterals.copy();
				remain.remove(first);
				if (removeSubsumedHelper(remain, child, nodesToDelete, comparator)) {
					if (remain.isEmpty() || child.getChildren().size() < 1) {
						nodesToDelete.addAll(child.findLeaves());
						it.remove();
						
						// Return but if the parent is the root
						if (child.getParent().getLabel() != null && child.getParent().getChildren().size() < 1)
							return true;
					}
				}
			} else if (comparator.compare(child.getLabel(), first) < 0) {
				// Must avoid concurrent modifications removing the child using the Iterator
				if (removeSubsumedHelper(remaingLiterals, child, nodesToDelete, comparator)) {
					if (child.getChildren().size() < 1) {
						nodesToDelete.addAll(child.findLeaves());
						it.remove();
						
						// Return but if the parent is the root
						if (child.getParent().getLabel() != null && child.getParent().getChildren().size() < 1)
							return true;
					}
				}
			}
		}
		
		
		if (!nodesToDelete.isEmpty() && node.getLabel() == null)
			return true;
		
		return false;
	}
	
}
