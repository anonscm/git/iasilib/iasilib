package fr.lri.iasi.libs.plogic.strategies;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;

/**
 * Describes the formula resolution strategy.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formulas to be resolved 
 * @param <F> The type of the formula element where the resolution will be applied.
 */
public interface ResolveStrategy<T extends NaryOperatorFormula<T, F>, F> {
	
	/**
	* Resolve between the current formula and the given one, on the given element (that belongs to the current formula).
	*
	* @param formula is the given formula
	* @param onElement is the element to resolve
	* @return a new formula representing the resolution between the current formula and the given one, on the given element.
	*/
	T performResolve(T formula, T otherFormula, F onElement);

}
