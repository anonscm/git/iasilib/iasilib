package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

/**
 * The default subsumption operation on a clause conjunction.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of conjunction where the subsumption operation will apply.
 */
public class ClauseCollectionSubsumesStrategy<T extends NaryOperatorFormula<T, Clause>> 
								implements SubsumesStrategy<T, Clause> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performSubsumes(T formula, Clause otherFormula) {
		if (otherFormula.isTautology())
			return true;
		
		for (Clause clause : formula) {
			if (clause.subsumes(otherFormula))
				return true;
		}
		
		return false;
	}

}
