package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.formallogic.strategy.InitializationStrategy;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryImpl;

/**
 * Implementation of the saturation strategy in a naive way. It only calls repeatedly the
 * selected consequence finding operation over each clause to be inserted on the theory. 
 * 
 * @author Andre Fonseca
 *
 */
public class NaiveSaturation implements InitializationStrategy<PropositionalClausalTheory, ConjunctionOfClauses> {

	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses initialize(PropositionalClausalTheory theory) {
		PropositionalClausalTheory newTheory = new PropositionalClausalTheoryImpl();
		// TODO set the consequence finding strategy
		
		ConjunctionOfClauses saturatedTheory = newTheory.produceConsequentsFrom(theory, null, false);
		
		return saturatedTheory;
	}

}
