package fr.lri.iasi.libs.plogic;

import java.util.Collection;
import java.util.Set;

import fr.lri.iasi.libs.formallogic.types.ConjunctionFormula;
import fr.lri.iasi.libs.plogic.util.Pair;

/**
 * A conjunction of clauses representation
 * 
 * @author Andre Fonseca
 *
 */
public interface ConjunctionOfClauses extends ConjunctionFormula<ConjunctionOfClauses, Clause> {

	/**
	* Returns true if the current set of formulas subsumes the given formula.
	*
	* @param formula
	* @return true if the current formula subsumes the given formula.
	*/
	boolean subsumes(Clause clause);
	
	/**
	* Returns true if the given formula was successfully added to the current formula set.
	* This operation will also subsumes existing formulas in the current formula set if 
	* the given formula is able to do it.
	*
	* @param formula : the formula to be added
	* @return a Pair containing two values: the first one is a boolean indicating true if the 
	* current formula was added. The second one is a list of the clauses subsumed by the added formula.
	*/
	ResultPair addSubsuming(Clause formula);
	
	/**
	* Returns true if the current formula set was modified by after the addition of the target formula set.
	* This operation will also subsumes existing formulas in the current formula set if 
	* the given formula is able to do it.
	*
	* @param set : the set of formulas to be added
	* @return a Pair containing two values: the first one is a boolean indicating true if at least one
	* formula in the input set was added. The second one is a list of the clauses subsumed by the added formulas.
	*/
	ResultPair addAllSubsuming(Collection<? extends Clause> set);
	
	/**
	* From a formula, not subsumed by the conjunction of clauses, removes all formulas
	* subsumed by the given formula.
	*
	* @param formula
	* @return the collection of subsumed clauses
	*/
	Collection<Clause> removeSubsumed(Clause formula);
	
	/**
	* From a set of formulas, not subsumed by the conjunction of clauses, removes all formulas
	* subsumed by each one of the given formulas.
	*
	* @param formula set
	* @return the collection of subsumed clauses
	*/
	Collection<Clause> removeAllSubsumed(Collection<? extends Clause> set);
	
	/**
	 * @return the delegate collection that represents the conjunction of clauses
	 */
	Set<Clause> delegate();
	
	/**
	 * Represents the tuple obtained as the result of the operation 'addSubsuming'.
	 * This tuple contains a boolean value (first) indicating if the target clause
	 * was correctly inserted and a collection of clauses (second) containing all
	 * clauses subsumed by the insertion of the target clause.
	 * 
	 */
	public class ResultPair extends Pair<Boolean, Collection<Clause>> {

		public ResultPair(Boolean first, Collection<Clause> second) {
			super(first, second);
		}

	}
	
}
