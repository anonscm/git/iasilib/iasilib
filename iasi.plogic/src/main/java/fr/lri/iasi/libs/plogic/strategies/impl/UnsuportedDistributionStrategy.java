package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;
import fr.lri.iasi.libs.plogic.DistributionStrategy;

/**
 * Describe a naive distribution strategy that throws an "UnsupportedOperationException"
 * @author Andre Fonseca
 *
 */
@SuppressWarnings("rawtypes")
public class UnsuportedDistributionStrategy implements DistributionStrategy {
	
	/**
	 * {@inheritDoc}
	 * 
	 * @throws UnsupportedOperationException
	 */
	@Override
	public NaryOperatorFormula performDistribution(NaryOperatorFormula formula, NaryOperatorFormula otherFormula) {
		throw new UnsupportedOperationException("Not implemented");
	}

}
