package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;

import com.google.common.collect.Lists;

import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;

/**
 * Clause addition operation on a clause conjunction (trie).
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseTrieAddFormulaStrategy implements AddFormulaStrategy<ConjunctionOfClauses, Clause>{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performAdd(ConjunctionOfClauses collection, Clause formula) {
		ResultPair result = performAddSubsuming(collection, formula);
    	return result.getFirst();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResultPair performAddSubsuming(ConjunctionOfClauses collection, Clause clause) {

		Collection<Clause> subsumedClauses = Lists.newLinkedList();
		
		if (clause.isTautology() || collection.isTautology())
			return new ResultPair(false, subsumedClauses);
		
		if (collection.subsumes(clause))
			return new ResultPair(false, subsumedClauses);
		
		if (!clause.isSat())
			collection.setSat(false);

		subsumedClauses = collection.removeSubsumed(clause);
		boolean addResult = collection.delegate().add(clause);
		
		return new ResultPair(addResult, subsumedClauses);
	}

}
