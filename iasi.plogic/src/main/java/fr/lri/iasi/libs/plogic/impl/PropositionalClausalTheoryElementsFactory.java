package fr.lri.iasi.libs.plogic.impl;

import fr.lri.iasi.libs.formallogic.strategy.ConsequenceFindingStrategy;
import fr.lri.iasi.libs.formallogic.strategy.InitializationStrategy;
import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;
import fr.lri.iasi.libs.plogic.strategies.impl.Ipia;
import fr.lri.iasi.libs.plogic.strategies.impl.NaiveSaturation;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;

/**
 * Create propositional logic objects depending of the configuration parameter "dataType". 
 * Implements the abstract factory and singleton patterns. 
 * 
 * @author Andre Fonseca
 *
 */
public final class PropositionalClausalTheoryElementsFactory {
	
	/* --- Properties --- */  
	/** The singleton object */
	private static PropositionalClausalTheoryElementsFactory instance = new PropositionalClausalTheoryElementsFactory();
	
	/** The initialization strategy to be used by all theories */
	private InitializationStrategy<PropositionalClausalTheory, ConjunctionOfClauses> initializationStrategyToBeUsed;
	
	/** The consequence finding strategy to be used by all theories */
	private ConsequenceFindingStrategy<PropositionalClausalTheory, ConjunctionOfClauses, Clause> consequenceFindindStrategyToBeUsed;
	
	/** The subsumption strategy to be used by all theories */
	private SubsumesStrategy<PropositionalClausalTheory, Clause> subsumesStrategyToBeUsed;
	
	/** The add fomula strategy to be used by all theories */
	private AddFormulaStrategy<PropositionalClausalTheory, Clause> addFormulaStrategyToBeUsed;
    
	/** The remove formula strategy to be used by all theories */
	private RemoveFormulaStrategy<PropositionalClausalTheory, Clause> removeFormulaStrategyToBeUsed;
	
	private PropositionalClausalTheoryElementsFactory() {
		StrategiesConfigurator.getInstance().configureStrategies(this);
		
		// Other default strategies
		this.consequenceFindindStrategyToBeUsed = new Ipia();
		this.initializationStrategyToBeUsed = new NaiveSaturation();
	}
	
	public static PropositionalClausalTheoryElementsFactory getInstance() {
		return instance;
	}
	
	/* --- Accessors --- */
	public InitializationStrategy<PropositionalClausalTheory, ConjunctionOfClauses> getSaturationStrategy() {
		return this.initializationStrategyToBeUsed;
	}
	
	public ConsequenceFindingStrategy<PropositionalClausalTheory, ConjunctionOfClauses, Clause> getConsequenceFindindStrategy() {
		return this.consequenceFindindStrategyToBeUsed;
	}
	
	public SubsumesStrategy<PropositionalClausalTheory, Clause> getSubsumesStrategy() {
		return subsumesStrategyToBeUsed;
	}
	
	public AddFormulaStrategy<PropositionalClausalTheory, Clause> getAddFormulaStrategy() {
		return addFormulaStrategyToBeUsed;
	}
	
	public RemoveFormulaStrategy<PropositionalClausalTheory, Clause> getRemoveFormulaStrategy() {
		return removeFormulaStrategyToBeUsed;
	}
	
	/* --- Mutators --- */
	public void setSaturationStrategy(InitializationStrategy<PropositionalClausalTheory, ConjunctionOfClauses> saturationStrategy) {
		this.initializationStrategyToBeUsed = saturationStrategy;
	}
	
	public void setConsequenceFindindStrategy(ConsequenceFindingStrategy<PropositionalClausalTheory, ConjunctionOfClauses, Clause> consequenceFindindStrategy) {
		this.consequenceFindindStrategyToBeUsed = consequenceFindindStrategy;
	}
	
	public void setAddFormulaStrategy(AddFormulaStrategy<PropositionalClausalTheory, Clause> addFormulaStrategy) {
		this.addFormulaStrategyToBeUsed = addFormulaStrategy;
	}
	
	public void setRemoveFormulaStrategy(RemoveFormulaStrategy<PropositionalClausalTheory, Clause> removeFormulaStrategy) {
		this.removeFormulaStrategyToBeUsed = removeFormulaStrategy;
	}
	
	public void setSubsumesStrategy(SubsumesStrategy<PropositionalClausalTheory, Clause> subsumesStrategy) {
		this.subsumesStrategyToBeUsed = subsumesStrategy;
	}

}
