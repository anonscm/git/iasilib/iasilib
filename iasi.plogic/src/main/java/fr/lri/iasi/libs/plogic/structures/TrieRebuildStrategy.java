package fr.lri.iasi.libs.plogic.structures;

import java.io.Serializable;

/**
 * The trie rebuild strategy interface. Indicates the conditions needed and
 * the rebuilding process.
 * 
 * @author Andre Fonseca
 *
 * @param <C> the type of the elements contained in the trie
 */
public interface TrieRebuildStrategy<C> extends Serializable {
	
	/**
	 * Need to be called when there is a modification on the trie. Analyze
	 * this modification and may call the rebuild operation.
	 * 
	 * @param isAdding : indicates if this modification is an addition
	 * @param params: The objects that are being inserted/removed
	 */
	void analyzeModification(boolean isAdding, C... params);
	
	/**
	 * Rebuild operation. May be called directly by the client or be called
	 * during the 'analyzeModification' operation.
	 */
	void rebuild();
	
	/**
	 * Reset the trie to its initial state. Normally, all the elements are
	 * removed and the modification counter, if any, is set to '0'.
	 */
	void reset();

}
