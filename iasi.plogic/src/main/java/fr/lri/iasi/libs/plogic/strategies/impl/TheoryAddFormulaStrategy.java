package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;
import java.util.Collections;

import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;

/**
 * The default clause addition operation on a propositional theory.
 * 
 * @author Andre Fonseca
 *
 */
public class TheoryAddFormulaStrategy implements AddFormulaStrategy<PropositionalClausalTheory, Clause> {

	/**
     * {@inheritDoc}
     * 
     * This operation will update literalHashTable accordingly.
	 */
	@Override
	public boolean performAdd(PropositionalClausalTheory collection,
			Clause formula) {
		
		if (!formula.isSat()) {
        	collection.getVocabulary().clear();
        	collection.getCanonicalMap().clear();
        }
		
		boolean result = collection.delegate().add(formula);

        if (result) {
        	
            for (Literal l : formula) {
            	Collection<Clause> cset = collection.getVocabularyFor(l);
                result &= cset.add(formula);
            }
            
            collection.getCanonicalMap().put(formula.canonicalCopy(), formula);
        }
    	
        return result;
	}

	/**
     * {@inheritDoc}
     * 
     * This operation will update literalHashTable accordingly.
	 */
	@Override
	public ResultPair performAddSubsuming(
			PropositionalClausalTheory collection, Clause clause) {
		Collection<Clause> subsumedClauses = Collections.emptyList();
		
		if (collection.subsumes(clause))
			return new ResultPair(false, subsumedClauses);
		
		subsumedClauses = collection.removeSubsumed(clause);

		boolean resultAdd = collection.add(clause);
        
		return new ResultPair(resultAdd, subsumedClauses);
	}

}
