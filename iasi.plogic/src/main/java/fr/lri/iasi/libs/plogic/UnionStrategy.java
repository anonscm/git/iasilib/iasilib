package fr.lri.iasi.libs.plogic;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;

/**
 * Describes the union of formula strategy.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formulas to be united 
 * @param <F> The type of the formula contained on the formulas to be united.
 */
public interface UnionStrategy<T extends NaryOperatorFormula<T, F>, F> {
	
	/**
	 * Performs the union operation on two formulas.
	 * 
	 * @param formula
	 * @param otherFormula
	 * @return a new formula representing the union of the two arg clauses.
	 */
	T performUnion(T formula, T otherFormula);

}
