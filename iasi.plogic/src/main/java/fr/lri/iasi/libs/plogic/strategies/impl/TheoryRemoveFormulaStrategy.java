package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseImpl;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;

/**
 * The default clause removal operation on a propositional theory.
 * 
 * @author Andre Fonseca
 *
 */
public class TheoryRemoveFormulaStrategy implements RemoveFormulaStrategy<PropositionalClausalTheory, Clause> {

	/** 
	 * {@inheritDoc}
	 * 
	 * NOTE: In order to avoid concurrent modifications exceptions when being used with
     * the class iterator, this overwritten method do not check if the clause was successfully
     * removed or not.
    */
	@Override
	public boolean performRemove(PropositionalClausalTheory collection,
			Object formula) {
		boolean result = collection.delegate().remove(formula);
    	
    	Clause clause = (ClauseImpl) formula;
    	for (Literal l : clause) {
    		Collection<Clause> cset = collection.getVocabularyFor(l);
    		result &= cset.remove(formula);
    		
    		if (cset.isEmpty())
    			collection.getVocabulary().remove(l);
    	}
    	
    	collection.getCanonicalMap().remove(clause.canonicalCopy());
    	
    	return result;
	}

	/**
     * {@inheritDoc}
     * 
     * NOTE : This operation, normally used from the delegate object,
     * was overwritten in the theory in order to profit from the 'literalIndex'
     * achieving a better performance.
     */
	@Override
	public Collection<Clause> performRemoveSubsumed(
			PropositionalClausalTheory collection, Clause formula) {
		Collection<Clause> result = new LinkedList<Clause>();
		
		// Build relevant clauses view
		Collection<Clause> relevantClauses = null;
		for (Literal lit : formula) {
			Collection<Clause> relatedClauses = collection.getVocabulary().get(lit);
			
			if (relatedClauses == null)
				continue;
			
			if (relevantClauses == null || (relevantClauses.size() > relatedClauses.size()))
				relevantClauses = relatedClauses;
		}
		
		if (!formula.isSat())
			relevantClauses = collection.copy();
		
		if (relevantClauses == null)
			return result;
		
		// Subsumes operation
		Iterator<Clause> it = relevantClauses.iterator();
		while (it.hasNext()) {
			Clause clause = it.next();
			if (formula.subsumes(clause)) {
				it.remove();
				collection.remove(clause);
				result.add(clause);
			}
		}
		
		return result;
	}

}
