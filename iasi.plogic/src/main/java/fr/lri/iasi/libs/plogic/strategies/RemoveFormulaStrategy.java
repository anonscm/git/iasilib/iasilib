package fr.lri.iasi.libs.plogic.strategies;

import java.util.Collection;
import java.util.Set;

/**
 * Strategy that describes the removal of a formula from its container formula.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the current formula (container)
 * @param <F> The type of the formula to be added (containing)
 */
public interface RemoveFormulaStrategy<T extends Set<F>, F> {
	
	/**
	 * Removes a formula its the container formula
	 * 
	 * @param collection : is the container formula
	 * @param formula : is the formula to be removed
	 * @return true if the formula was correctly removed, false otherwise
	 */
	boolean performRemove(T collection, Object formula);
	
	/**
	 * Removes all formulas from a container formula, that are subsumed by the parameter formula.
	 * 
	 * @param collection : is the container formula
	 * @param clause : is the formula used for the subsumption tests.
	 * @return a collection of all formulas removed from the container formula.
	 */
	Collection<F> performRemoveSubsumed(T collection, F formula);

}
