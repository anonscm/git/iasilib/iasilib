package fr.lri.iasi.libs.plogic.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import com.google.common.collect.ForwardingSet;
import com.google.common.collect.Maps;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.formallogic.strategy.ConsequenceFindingStrategy;
import fr.lri.iasi.libs.formallogic.strategy.InitializationStrategy;
import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

/**
 * Abstract propositional theory. Any propositional theory must extend this class any affect the attribute
 * "delegate" in order to be represented on Somewhere.
 *  
 * @author Andre Fonseca
 *
 */
public class PropositionalClausalTheoryImpl extends ForwardingSet<Clause> implements PropositionalClausalTheory {
	private static final long serialVersionUID = 1623651759638080500L;
	
	/* --- Properties --- */
	/** The delegate collection */
	protected ConjunctionOfClauses delegate;
	
	/** The label used for current conjunction of clauses (can be null) */
    protected String label;
	
    /** Hash Table thats associate a literal to the clauses of this theory that contains it */
	protected final Map<Literal, Collection<Clause>> literalIndex = Maps.newHashMap();
	
	/** Hash Table thats associate a canonical form a clause with of a containing clause */
	protected final Map<Clause, Clause> canonicalIndex = Maps.newHashMap();
	
	/** The consequence finding strategy used by this theory */
	protected transient ConsequenceFindingStrategy<PropositionalClausalTheory, ConjunctionOfClauses, Clause> consequenceFindingStrategy;
	
	/** The initialization strategy used by this theory */
	protected transient InitializationStrategy<PropositionalClausalTheory, ConjunctionOfClauses> initializationStrategy;
	
	/** The subsumes strategy to be used by the current theory */
    protected transient SubsumesStrategy<PropositionalClausalTheory, Clause> subsumesStrategy;
    
    /** The strategy to be used for adding formulas into this theory */
    protected transient AddFormulaStrategy<PropositionalClausalTheory, Clause> addFormulaStrategy;
    
    /** The strategy to be used for removing formulas into this theory */
    protected transient RemoveFormulaStrategy<PropositionalClausalTheory, Clause> removeFormulaStrategy;
	
    public PropositionalClausalTheoryImpl() {
        this.delegate = new ConjunctionOfClausesImpl();
        this.subsumesStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getSubsumesStrategy();
		this.addFormulaStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getAddFormulaStrategy();
		this.removeFormulaStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getRemoveFormulaStrategy();
		
		Random r = new Random();
		this.label = new UUID(r.nextLong(), r.nextLong()).toString();
    }

    public PropositionalClausalTheoryImpl(Collection<?extends Clause> c) {
        this();
        addAll(c);
    }
    
    /* --- Accessors --- */
	@Override
	public String getLabel() {
		return label;
	}
	
    @Override
	public Map<Literal, Collection<Clause>> getVocabulary() {
		return literalIndex;
	}
    
    /**
     * {@inheritDoc}
     */
	@Override
	public boolean isTautology() {
		return this.delegate.isTautology();
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public boolean isSat() {
		return this.delegate.isSat();
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Map<String, Annotation<?, ?>> getAnnotations() {
		return this.delegate.getAnnotations();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Clause, Clause> getCanonicalMap() {
		return canonicalIndex;
	}
    
    /* --- Mutators --- */
    public void setId(String id) {
		this.label = id;
	}
    
    /**
     * {@inheritDoc}
     */
    @Override
	public void setTautology(boolean isTautology) {
		this.delegate.setTautology(isTautology);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public void setSat(boolean isSat) {
		this.delegate.setSat(isSat);
	}
    
    /* --- Methods --- */
    @Override
    public boolean addAll(Collection<? extends Clause> c) {
    	boolean result = false;
		
		for (Clause clause : c) {
			result |= this.add(clause);
		}

		return result;
	}
    
    @Override
	public boolean removeAll(Collection<?> c) {
    	boolean modified = false;

		for (Object object : c) {
			modified |= this.remove(object);
		}

		return modified;
	}
    
    /**
     * Add a new clause, and update literalHashTable accordingly
     * @param c Clause to add
     * @return true if success
     */
    @Override
    public boolean add(Clause c) {
    	return this.addFormulaStrategy.performAdd(this, c);
    }
    
    /**
     * Remove a new clause, and update literalHashTable accordingly
     * @param c Clause to remove
     * @return true if success
     */
    @Override
    public boolean remove(Object c) {
    	return this.removeFormulaStrategy.performRemove(this, c);
    }
    
    /**
     * {@inheritDoc}
     */
	@Override
	public ResultPair addAllSubsuming(Collection<? extends Clause> set) {
		Collection<Clause> subsumedClauses = null;
		boolean totalResult = false;
		
		for (Clause clause : set) {
			ResultPair pair = addSubsuming(clause);
			totalResult |= pair.getFirst();
			
			if (subsumedClauses == null)
				subsumedClauses = pair.getSecond();
			else
				subsumedClauses.addAll(pair.getSecond());
		}
		
		return new ResultPair(totalResult, subsumedClauses);
	}
	
	/**
     * {@inheritDoc}
     */
    @Override
    public ResultPair addSubsuming(Clause c) {	
    	return this.addFormulaStrategy.performAddSubsuming(this, c);
    }
    
    /**
     * {@inheritDoc}
     */
	@Override
	public Collection<Clause> removeAllSubsumed(Collection<? extends Clause> set) {
		Collection<Clause> result = null;
		
		for (Clause clause : set) {
			if (result == null)
				result = removeSubsumed(clause);
			else
				result.addAll(removeSubsumed(clause));
		}
		
		return result;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Collection<Clause> removeSubsumed(Clause formula) {
		return this.removeFormulaStrategy.performRemoveSubsumed(this, formula);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public PropositionalClausalTheory copy() {
		return new PropositionalClausalTheoryImpl(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PropositionalClausalTheory canonicalCopy() {
		PropositionalClausalTheory result = new PropositionalClausalTheoryImpl(this);
		result.getAnnotations().clear();
		
		return result;
	}

    /**
	 * Verifies if the literal is on the vocabulary.
	 * @param index literal to be checked
	 * @return true it is contained on the vocabulary, false otherwise.
	 */
    @Override
	public boolean isInVocabulary(Literal index) {
		return literalIndex.containsKey(index);
	}
    
    /**
     * Get clauses that contains a given literal
     * If the entry don't exist in literalHashTable, it will be created
     * @param l the given literal
     * @return clauses that contains that literal
     */
	@Override
	public Collection<Clause> getVocabularyFor(Literal index) {
		Collection<Clause> result = this.literalIndex.get(index);

        if (result == null) {
            result = new LinkedHashSet<Clause>();
            this.literalIndex.put(index, result);
        }

        return result;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses produceConsequentsFrom(
			ConjunctionOfClauses clausesToAdd, ConjunctionOfClauses alreadyAdded, boolean onlyNewConsequents) {
		this.consequenceFindingStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getConsequenceFindindStrategy();
		this.consequenceFindingStrategy.onlyNewConsequentResults(onlyNewConsequents);
		return this.consequenceFindingStrategy.process(this, clausesToAdd, alreadyAdded);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConjunctionOfClauses produceConsequentsFrom(
			ConjunctionOfClauses clausesToAdd, boolean onlyNewConsequents) {
		this.consequenceFindingStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getConsequenceFindindStrategy();
		this.consequenceFindingStrategy.onlyNewConsequentResults(onlyNewConsequents);
		return this.consequenceFindingStrategy.process(this, clausesToAdd, null);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses initialize() {
		this.initializationStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getSaturationStrategy();
		
		return this.initializationStrategy.initialize(this);
	}


	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses union(ConjunctionOfClauses formula) {
		return this.delegate.union(formula);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean subsumes(Clause clause) {
		return this.subsumesStrategy.performSubsumes(this, clause);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public void addAnnotation(Annotation<?, ?> annotation) {
		this.delegate.addAnnotation(annotation);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses distribution(ConjunctionOfClauses formula) {
		return this.delegate.distribution(formula);
	}
	
	@Override
	public Set<Clause> delegate() {
		return this.delegate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result;
		return result;
	}

	@Override
	public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof Set<?>))
            return false;
        
        if (o instanceof PropositionalClausalTheory) {
        	PropositionalClausalTheoryImpl c = (PropositionalClausalTheoryImpl) o;
        	return delegate.equals(c.delegate());
        }
        
        return false;
	}
	
	@Override
	public Iterator<Clause> iterator() {
		return new PropositionalTheoryIterator(((ConjunctionOfClausesImpl)this.delegate).iterator());
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeUTF(label);
		out.writeObject(delegate);
		out.writeObject(literalIndex);
		out.writeObject(canonicalIndex);
	}
	
	/**
	 * Transient field cannot be null on deserialization.
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException, 
														SecurityException, NoSuchFieldException, 
														IllegalArgumentException, IllegalAccessException {
		
		this.label = in.readUTF();
		
		Field fDelegate = PropositionalClausalTheoryImpl.class.getDeclaredField("delegate");
		fDelegate.set(this, in.readObject());
        
        Field fLiteralIndex = PropositionalClausalTheoryImpl.class.getDeclaredField("literalIndex");
        fLiteralIndex.setAccessible(true);
        fLiteralIndex.set(this, in.readObject());
        
        Field fcanonicalIndex = PropositionalClausalTheoryImpl.class.getDeclaredField("canonicalIndex");
        fcanonicalIndex.setAccessible(true);
        fcanonicalIndex.set(this, in.readObject());
        
        this.consequenceFindingStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getConsequenceFindindStrategy();
        this.initializationStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getSaturationStrategy();
		this.subsumesStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getSubsumesStrategy();
		this.addFormulaStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getAddFormulaStrategy();
		this.removeFormulaStrategy = PropositionalClausalTheoryElementsFactory.getInstance().getRemoveFormulaStrategy();

    }
	
	/**
	 * Iterator used to extend the remove operation.
	 * 
	 * @author Andre Fonseca
	 *
	 */
	public class PropositionalTheoryIterator implements Iterator<Clause> {

		protected Iterator<Clause> realIterator;

		protected Clause current;

		public PropositionalTheoryIterator(Iterator<Clause> delegate) {
			if (delegate == null) {
				throw new IllegalArgumentException("realIterator");
			}

			this.realIterator = delegate;
		}

		public boolean hasNext() {
			return this.realIterator.hasNext();
		}

		public Clause next() {
			this.current = this.realIterator.next();
			return current;
		}

		public void remove() {
			this.realIterator.remove();
			
			PropositionalClausalTheoryImpl.this.remove(current);
		}
	}
	
}
