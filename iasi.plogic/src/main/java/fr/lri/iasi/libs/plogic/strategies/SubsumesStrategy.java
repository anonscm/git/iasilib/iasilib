package fr.lri.iasi.libs.plogic.strategies;

/**
 * Strategy that describes the subsumption test between formulas.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the first argument formula
 * @param <F> The type of the second argument formula
 */
public interface SubsumesStrategy<T, F> {
	
	/**
	* Performs the subsumption operation on the given two formulas.
	*
	* @param formula
	* @param otherFormula
	* @return true if formula subsumes otherFormula. false otherwise.
	*/
	boolean performSubsumes(T formula, F otherFormula);

}
