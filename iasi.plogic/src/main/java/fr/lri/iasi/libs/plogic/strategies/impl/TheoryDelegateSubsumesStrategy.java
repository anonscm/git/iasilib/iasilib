package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

/**
 * Describes a clause subsumption operation on a propositional theory, delegating the
 * subsumption test to its inner conjunction of clauses.
 * 
 * @author Andre Fonseca
 *
 */
public class TheoryDelegateSubsumesStrategy implements SubsumesStrategy<PropositionalClausalTheory, Clause> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performSubsumes(PropositionalClausalTheory formula,
			Clause otherFormula) {
		
		ConjunctionOfClauses delegate = (ConjunctionOfClauses) formula.delegate();
		return delegate.subsumes(otherFormula);
	}

}
