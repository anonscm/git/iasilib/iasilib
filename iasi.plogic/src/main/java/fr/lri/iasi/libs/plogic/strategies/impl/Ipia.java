package fr.lri.iasi.libs.plogic.strategies.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import fr.lri.iasi.libs.formallogic.strategy.ConsequenceFindingStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesImpl;

/**
 * IPIA Consequence Finding Strategy. This is an implementation of the algorithm presented on 
 * [Kean, A. and Tsiknis G.,<i> An Incremental Method for Generating Prime Implicants/Implicates </i>, 
 * <i>Journal of Symbolic Computation</i>, <b>1990</b>, 9:185-206] and [Kean, A. and Tsiknis G.,<i> 
 * A Corrigendum fro the Optimized-IPIA </i>, <b>1994</b>].
 * 
 * @author Andre Fonseca
 *
 */
public class Ipia implements ConsequenceFindingStrategy<PropositionalClausalTheory, ConjunctionOfClauses, Clause> {
	
	/** The consequence finding operation cache */
    private final Cache<CachePair, ConjunctionOfClauses> newConsequentsCache;
    
    /** The consequence finding operation cache */
    private final Cache<CachePair, ConjunctionOfClauses> saturatedTheoryCache;
    
    private boolean onlyNewConsequents;
    
    public Ipia() {
    	this.newConsequentsCache = createCache();
    	this.saturatedTheoryCache = createCache();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
	public void onlyNewConsequentResults(boolean onlyConsequents) {
		this.onlyNewConsequents = onlyConsequents;
	}
    
    /**
     * NOTE: The default write expiration on cache is 60s.
     * 
     * @return a generic execution cache to be used, for example, by
     * the consequence finding operation.
     */
    private <T, F> Cache<T, F> createCache() {
    	// Create cache
        return CacheBuilder.newBuilder()
				.maximumSize(1000) //TODO : Hardcoded
				.expireAfterAccess(1, TimeUnit.MINUTES) //TODO : Hardcoded
				.build();
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses process(PropositionalClausalTheory initialTheoryRef, ConjunctionOfClauses clausesToAdd, ConjunctionOfClauses clausesAlreadyAdded) {
		
		// At first, try to find result in the cache
		ConjunctionOfClauses result = null;
		ConjunctionOfClauses clauses = clausesToAdd;
		if (clausesAlreadyAdded != null) {
			clauses = clausesToAdd.copy();
			clauses.addAll(clausesAlreadyAdded);
		}
		
		ConjunctionOfClauses cachedResult = this.newConsequentsCache.getIfPresent(new CachePair(clauses, initialTheoryRef.getLabel()));
		if (cachedResult == null) {
			
			// If does not exists on the cache, try to evaluate if some of the work was done in previous steps.
			// This is done by considering only the result from the clauses already added to the theory
			ConjunctionOfClauses cachedPartialResult = null;
			if (clausesAlreadyAdded != null)
				cachedPartialResult = this.saturatedTheoryCache.getIfPresent(new CachePair(clausesAlreadyAdded, initialTheoryRef.getLabel()));
			
			if (cachedPartialResult != null) {
				initialTheoryRef = (PropositionalClausalTheory) cachedPartialResult;
				result = findConsequents(initialTheoryRef, clausesToAdd);
			} else {
				// If none, process the whole input
				result = findConsequents(initialTheoryRef, clauses);
			}
			
			// Put the whole saturated theory on the cache
			this.saturatedTheoryCache.put(new CachePair(clauses, initialTheoryRef.getLabel()), result.copy());
			
			// Removing initial theory from results if necessary
			if (this.onlyNewConsequents) {
				result.removeAll(initialTheoryRef);
				
				// Put only the new consequents results on the cache
				this.newConsequentsCache.put(new CachePair(clauses, initialTheoryRef.getLabel()) , result.copy());
			}
		} else {
			result = cachedResult.copy();
		}
		
		return result;
	}
	

	public ConjunctionOfClauses findConsequents(PropositionalClausalTheory initialTheoryRef, Collection<Clause> clauses) {
		// Defensive copy of clausesToAdd, sorting its contents
		PropositionalClausalTheory initialTheory = (PropositionalClausalTheory) initialTheoryRef.copy();
		List<Clause> clausesToAdd = Lists.newArrayList(clauses);
		Collections.sort(clausesToAdd, new SmallestClauseComparator());
		
		Iterator<Clause> it = clausesToAdd.iterator();
		while (it.hasNext()) {
			Clause clause = it.next();
			
			// Delete clauses from theory + clause that can be subsumed by other internal formula
			if (initialTheory.subsumes(clause)) {
				it.remove();
				continue;
			}
			
			initialTheory.removeSubsumed(clause);
			
			// Root optimization
			clause = rootOptimization(initialTheory, clause);
			
			// If the result of the root optimization is a contradiction, subsumes everything and returns!
			if (!clause.isSat()) {
				initialTheory.addSubsuming(clause);
				return initialTheory;
			}
			
			// Let sigma be the set containing the produced prime implicates
			ConjunctionOfClauses sigma = new ConjunctionOfClausesImpl();
			Multimap<Clause, Literal> restrictionIndex = HashMultimap.create();
			
			sigma.add(clause);
			
			// Loop must be a label in order to be stopped
			for (Literal literal : clause) {
				
				// Compute the resolution of clauses in sigma with resolvingClausesIndex...
				Collection<Clause> resolvingClauses = Lists.newArrayList(initialTheory.getVocabularyFor(literal.opposite()));
				
				ConjunctionOfClauses sigmaChildren = new ConjunctionOfClausesImpl();
				
				for (Clause resolvingClause : resolvingClauses) {
					
					// Use iterator on sigma in order to avoid concurrent modifications
					Iterator<Clause> itSigma = sigma.iterator();
					
					ConjunctionOfClauses sigmaClauseChildren = new ConjunctionOfClausesImpl();
					
					while (itSigma.hasNext()) {
						Clause sigmaClause = itSigma.next();
						
						Clause resolution = sigmaClause.resolve(resolvingClause, literal);
						
						// If the resolution is not possible or it has generated a tautology, ignore the resolution
						if (resolution == null || resolution.isTautology())
							continue;
						
						// If there is a contradiction, subsumes everything and returns!
						if (!resolution.isSat()) {
							initialTheory.addSubsuming(resolution);
							return initialTheory;
						}
						
						Collection<Literal> restriction = restrictionIndex.get(sigmaClause);
						boolean noLiteralsIntersection = Collections.disjoint(restriction, resolvingClause);
												
						if (noLiteralsIntersection) {
							Collection<Literal> resolutionRestriction = restrictionIndex.get(resolution);
							resolutionRestriction.clear();
							resolutionRestriction.addAll(restriction);
							
							if (resolution.subsumes(sigmaClause))
								itSigma.remove();
							else
								resolutionRestriction.add(literal);
							
							sigmaClauseChildren.addSubsuming(resolution);
						}
					}
					
					// Local subsumption
					initialTheory.removeAllSubsumed(sigmaClauseChildren);
					sigmaClauseChildren.removeAllSubsumed(initialTheory);
					
					if (!sigmaClauseChildren.isEmpty()) {
						for (Clause clauseChildren : sigmaClauseChildren) {
							ResultPair result = sigmaChildren.addSubsuming(clauseChildren);
							if (result.getFirst()) {
								Collection<Literal> restriction = restrictionIndex.get(clauseChildren);
								for (Clause c : result.getSecond()) {
									Collection<Literal> restrictionSubsumed = restrictionIndex.get(c);
					            	restriction.retainAll(restrictionSubsumed);
								}
							}
						}
					}
				}
				
				sigma.addAll(sigmaChildren);
			}
			
			initialTheory.addAll(sigma);
		}
		
		return initialTheory;
	}
	
	/**
	 * The root optimization process aims to subsume elements from the initial theory
	 * based on the input clause. This process also verifies if the input clause can
	 * be subsumed by the existing theory, eliminating it from the input set.
	 * 
	 * @param input clause
	 * @return clause if not subsumed
	 */
	protected Clause rootOptimization(PropositionalClausalTheory initialTheory, Clause clause) {		
		Clause tmpClause = clause.copy();
		for (Literal literal : tmpClause) {
			// Get the clauses that can be resolved by this literal
			Collection<Clause> resolvingClauses = initialTheory.getVocabularyFor(literal.opposite());
			
			for (Clause resolvingClause : resolvingClauses) {
				Clause resolution = clause.resolve(resolvingClause, literal);
				if (resolution != null && !resolution.isTautology() && resolution.isSat()) {
					if (resolution.subsumes(clause)) {
						
						// Deletes any clause in the theory that is subsumed by the subsuming resolution
						initialTheory.removeSubsumed(resolution);
						
						return resolution;
					}
				} else if (resolution != null && !resolution.isSat()) {
					return resolution;
				}
			}
		}
		
		return clause;
	}
	
	/**
     *  Comparator based on the size of a clause.
     *  
     *  @author Andre Fonseca
     */
    private static class SmallestClauseComparator implements Comparator<Clause>, Serializable {
		private static final long serialVersionUID = 6253601901704413890L;

		// Comparator interface requires defining compare method.
        public int compare(Clause clause1, Clause clause2) {
        	Integer sizeClause1 = Integer.valueOf(clause1.size());
        	Integer sizeClause2 = Integer.valueOf(clause2.size());
        	
            return (sizeClause1.compareTo(sizeClause2));
        }
    }
    
    /**
     * Represents an entry on the Ipia cache, referencing the used input and the target theory.
     * That helper class was necessary to separate the cache for each theory, avoiding conflicts between different
     * query executions.
     * 
     * @author Andre Fonseca
     *
     */
    private class CachePair {

		/** Consequence finding input */
		private Collection<Clause> input;
    	
    	/** Referenced theory */
		private String theoryId;

		public CachePair(Collection<Clause> input, String theoryId) {
			super();
			this.input = input;
			this.theoryId = theoryId;
		}

		private Ipia getOuterType() {
			return Ipia.this;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((input == null) ? 0 : input.hashCode());
			result = prime * result
					+ ((theoryId == null) ? 0 : theoryId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CachePair other = (CachePair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (input == null) {
				if (other.input != null)
					return false;
			} else if (!input.equals(other.input))
				return false;
			if (theoryId == null) {
				if (other.theoryId != null)
					return false;
			} else if (!theoryId.equals(other.theoryId))
				return false;
			return true;
		}
    }

}
