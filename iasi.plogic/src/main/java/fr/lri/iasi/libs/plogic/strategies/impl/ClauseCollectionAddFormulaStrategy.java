package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses.ResultPair;

/**
 * Clause addition operation on a clause conjunction (hashset).
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseCollectionAddFormulaStrategy implements AddFormulaStrategy<ConjunctionOfClauses, Clause> {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performAdd(ConjunctionOfClauses collection, Clause formula) {
		if (!collection.isSat()) {
			Clause emptyClause = Iterables.getFirst(collection, null);
			if (emptyClause != null && emptyClause.subsumes(formula))
				return false;
		}
		
		if (!formula.isSat()) {
			collection.setSat(false);
			collection.clear();
		}
		
		if (formula.isTautology() || collection.isTautology())
			return false;
		
		return collection.delegate().add(formula);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResultPair performAddSubsuming(ConjunctionOfClauses collection,
			Clause clause) {
        
        Collection<Clause> subsumedClauses = Lists.newLinkedList();
		
		if (clause.isTautology() || collection.isTautology())
			return new ResultPair(false, subsumedClauses);
		
		if (!clause.isSat())
			collection.setSat(false);
		
		if (collection.subsumes(clause))
			return new ResultPair(false, subsumedClauses);
		
        subsumedClauses = collection.removeSubsumed(clause);
        boolean addResult = collection.add(clause);
        
        return new ResultPair(addResult, subsumedClauses);
	}

}
