package fr.lri.iasi.libs.plogic.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.google.common.collect.ForwardingSet;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.DistributionStrategy;
import fr.lri.iasi.libs.plogic.UnionStrategy;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

/**
 * Abstract conjunction of clauses. Handle common operations used by the different implementations of
 * the conjunction of clauses on Somewhere. Needs to define a delegate collection.
 * 
 * @author Andre Fonseca
 *
 */
public class ConjunctionOfClausesImpl extends ForwardingSet<Clause> implements ConjunctionOfClauses {
	private static final long serialVersionUID = -6487483658450860815L;

	/* --- Properties --- */
	/** The delegate collection */
	protected Set<Clause> delegate;
	
	/** The label used for current conjunction of clauses (can be null) */
    protected String label = "";
	
    /** Indicates if the current cnf is satisfiable */
	protected boolean isSatisfiable = true;
    
	/** Indicates if the current cnf is a tautology */
    protected boolean isTautology = false;
    
    /** Map of annotations of the current cnf */
    protected final Map<String, Annotation<?, ?>> annotations = new HashMap<String, Annotation<?, ?>>();
	
	/** The distribution strategy to be used by the current conjunction of clauses */
    protected transient DistributionStrategy<ConjunctionOfClauses, Clause> distributionStrategy;
	
    /** The union strategy to be used by the current conjunction of clauses */
    protected transient UnionStrategy<ConjunctionOfClauses, Clause> unionStrategy;
    
    /** The subsumption strategy to be used by the current conjunction of clauses */
    protected transient SubsumesStrategy<ConjunctionOfClauses, Clause> subsumesStrategy;
    
    /** The strategy to be used by the current conjunction of clauses when adding formula to it */
    protected transient AddFormulaStrategy<ConjunctionOfClauses, Clause> addFormulaStrategy;
    
    /** The strategy to be used by the current conjunction of clauses when removing formula from it */
    protected transient RemoveFormulaStrategy<ConjunctionOfClauses, Clause> removeFormulaStrategy;
    
    /* --- Properties --- */
	/** The delegate Trie object */
    public ConjunctionOfClausesImpl() {
    	this.delegate = ConjunctionOfClausesElementsFactory.getInstance().createElement();
    	this.distributionStrategy = ConjunctionOfClausesElementsFactory.getInstance().getDistributionStrategy();
		this.unionStrategy = ConjunctionOfClausesElementsFactory.getInstance().getUnionStrategy();
		this.subsumesStrategy = ConjunctionOfClausesElementsFactory.getInstance().getSubsumesStrategy();
		this.addFormulaStrategy = ConjunctionOfClausesElementsFactory.getInstance().getAddFormulaStrategy();
		this.removeFormulaStrategy = ConjunctionOfClausesElementsFactory.getInstance().getRemoveFormulaStrategy();
    }
    
    public ConjunctionOfClausesImpl(ConjunctionOfClauses c) {
    	this();
    	this.addAllSubsuming(c);
    	this.isSatisfiable = c.isSat();
		this.isTautology = c.isTautology();
		
		for (Entry<String, Annotation<?, ?>> entry : c.getAnnotations().entrySet()) {
			this.annotations.put(entry.getKey(), entry.getValue().copy());
		}
    }
    
    public ConjunctionOfClausesImpl(Collection<Clause> c) {
		this();
		this.addAllSubsuming(c);
	}
	
	/* --- Accessors --- */
    @Override
	public String getLabel() {
		return label;
	}
    
	@Override
	public Map<String, Annotation<?, ?>> getAnnotations() {
		return this.annotations;
	}
	
	@Override
	public boolean isTautology() {
		return isTautology;
	}

	@Override
	public boolean isSat() {
		return isSatisfiable;
	}
	
	/* --- Mutators --- */
    public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public void addAnnotation(Annotation<?, ?> annotation) {
		if (this.annotations.containsKey(annotation.getName())) {
			Annotation<?, ?> oldAnnotation = this.annotations.get(annotation.getName());
			oldAnnotation.addContent(oldAnnotation.getContent());
		} else {
			this.annotations.put(annotation.getName(), annotation);
		}
	}
	
	@Override
	public void setTautology(boolean isTautology) {
		this.isTautology = isTautology;
	}

	@Override
	public void setSat(boolean isSat) {
		this.isSatisfiable = isSat;
	}

	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses distribution(ConjunctionOfClauses set) {
		return this.distributionStrategy.performDistribution(this, set);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses union(ConjunctionOfClauses set) {
		return this.unionStrategy.performUnion(this, set);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean subsumes(Clause formula) {
		return this.subsumesStrategy.performSubsumes(this, formula);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public ResultPair addSubsuming(Clause formula) {
		return this.addFormulaStrategy.performAddSubsuming(this, formula);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Collection<Clause> removeSubsumed(Clause formula) {
		return this.removeFormulaStrategy.performRemoveSubsumed(this, formula);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public ResultPair addAllSubsuming(Collection<? extends Clause> set) {
		Collection<Clause> subsumedClauses = null;
		boolean totalResult = false;
		
		for (Clause clause : set) {
			ResultPair pair = addSubsuming(clause);
			totalResult |= pair.getFirst();
			
			if (subsumedClauses == null)
				subsumedClauses = pair.getSecond();
			else
				subsumedClauses.addAll(pair.getSecond());
		}
		
		return new ResultPair(totalResult, subsumedClauses);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Collection<Clause> removeAllSubsumed(Collection<? extends Clause> set) {
		Collection<Clause> result = null;
		
		for (Clause clause : set) {
			if (result == null)
				result = removeSubsumed(clause);
			else
				result.addAll(removeSubsumed(clause));
		}
		
		return result;
	}
	
	/**
     * Generates a XML representation string of the object
     * @return XML representation string
     */
	public String toXml() {
		StringBuilder res = new StringBuilder();
        res.append("<theory>");

        for (Clause c : this) {
        	res.append(c);
        	res.append(" ");
        }

        res.append("</theory>");

        return res.toString();
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Set<Clause> delegate() {
		return delegate;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public ConjunctionOfClauses copy() {
		return new ConjunctionOfClausesImpl(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConjunctionOfClauses canonicalCopy() {
		ConjunctionOfClauses result = new ConjunctionOfClausesImpl(this);
		result.getAnnotations().clear();
		
		return result;
	}
	
	@Override
	public boolean add(Clause clause) {
		return this.addFormulaStrategy.performAdd(this, clause);
	}
	
	@Override
	public boolean remove(Object object) {
		return this.removeFormulaStrategy.performRemove(this, object);
	}
	
	@Override
	public Iterator<Clause> iterator() {
		return delegate.iterator();
	}
	
	@Override
    public String toString() {
		StringBuilder res = new StringBuilder();
		res.append("(");

        for (Clause c : this) {
        	res.append(c);
        	res.append(" \u2227 ");
        }
        
        if (res.length() > 1)
        	res.delete(res.length() - 3, res.length());
        
        res.append(")");

        return res.toString();
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + annotations.hashCode();
		result = prime * result + (isSatisfiable ? 1231 : 1237);
		result = prime * result + (isTautology ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof Set<?>))
            return false;
        
        if (o instanceof ConjunctionOfClausesImpl) {
        	ConjunctionOfClausesImpl c = (ConjunctionOfClausesImpl) o;
        	
        	return delegate.equals(c.delegate()) && 
        			annotations.equals(c.getAnnotations()) &&
        			isSatisfiable == c.isSat() && isTautology == c.isTautology();
        }
        
        return false;
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeUTF(label);
		out.writeBoolean(isSatisfiable);
		out.writeBoolean(isTautology);
		out.writeObject(annotations);
		out.writeObject(delegate);
	}
	
	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		
		this.label = in.readUTF();
		this.isSatisfiable = in.readBoolean();
		this.isTautology = in.readBoolean();
		
		Field fAnnotations = ConjunctionOfClausesImpl.class.getDeclaredField("annotations");
		fAnnotations.setAccessible(true);
		fAnnotations.set(this, in.readObject());
		
		Field fDelegate = ConjunctionOfClausesImpl.class.getDeclaredField("delegate");
		fDelegate.set(this, in.readObject());
		
		this.distributionStrategy = ConjunctionOfClausesElementsFactory.getInstance().getDistributionStrategy();
		this.unionStrategy = ConjunctionOfClausesElementsFactory.getInstance().getUnionStrategy();
		this.subsumesStrategy = ConjunctionOfClausesElementsFactory.getInstance().getSubsumesStrategy();
		this.addFormulaStrategy = ConjunctionOfClausesElementsFactory.getInstance().getAddFormulaStrategy();
		this.removeFormulaStrategy = ConjunctionOfClausesElementsFactory.getInstance().getRemoveFormulaStrategy();
	}

}
