package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;
import java.util.List;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.PropositionalClausalTheory;
import fr.lri.iasi.libs.plogic.impl.ClauseImpl;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;
import fr.lri.iasi.libs.plogic.structures.ClauseTrieNode;

/**
 * The clause removal operation on a propositional theory (trie).
 * 
 * @author Andre Fonseca
 *
 */
public class TheoryTrieRemoveFormulaStrategy implements RemoveFormulaStrategy<PropositionalClausalTheory, Clause> {

	/**
	 * {@inheritDoc}
	 * 
     * This operation will update literalHashTable accordingly.
     */
	@Override
	public boolean performRemove(PropositionalClausalTheory collection, Object formula) {
		boolean result = collection.delegate().remove(formula);
        
    	if (formula instanceof Clause) {
    		
        	Clause clause = (ClauseImpl) formula;
            for (Literal l : clause) {
            	Collection<Clause> cset = collection.getVocabularyFor(l);
                result &= cset.remove(formula);
                
                if (cset.isEmpty())
                	collection.getVocabulary().remove(l);
                
                collection.getCanonicalMap().remove(clause.canonicalCopy());
            }
    	} else if (formula instanceof ClauseTrieNode) {
    		ClauseTrieNode node = (ClauseTrieNode) formula;
    		List<Clause> clauses = node.findLeaves();
    		
    		for (Clause clause : clauses) {
    			
        		for (Literal l : clause) {
        			Collection<Clause> cset = collection.getVocabularyFor(l);
	                result &= cset.remove(clause);
	                
	                if (cset.isEmpty())
	                	collection.getVocabulary().remove(l);
	            }
        		
        		collection.getCanonicalMap().remove(clause.canonicalCopy());
    		}
    	}

        return result;
	}

	/**
	 * {@inheritDoc}
	 * 
     * This operation will update literalHashTable accordingly.
     */
	@Override
	public Collection<Clause> performRemoveSubsumed(
			PropositionalClausalTheory collection, Clause formula) {
		
		ConjunctionOfClauses delegate = (ConjunctionOfClauses) collection.delegate();
		Collection<Clause> removedClauses = delegate.removeSubsumed(formula);
		
		for (Clause clause : removedClauses) {
			for (Literal l : clause) {
            	Collection<Clause> cset = collection.getVocabularyFor(l);
                cset.remove(clause);
                
                if (cset.isEmpty())
                	collection.getVocabulary().remove(l);
                
                collection.getCanonicalMap().remove(clause.canonicalCopy());
            }
		}
		
		return removedClauses;
	}

}
