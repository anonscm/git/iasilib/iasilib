package fr.lri.iasi.libs.plogic.impl;

import com.google.common.collect.ForwardingSet;
import com.google.common.collect.Ordering;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.DistributionStrategy;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.UnionStrategy;
import fr.lri.iasi.libs.plogic.strategies.ResolveStrategy;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Describes a propositional clause
 * 
 * @author Andre Fonseca
 * @author Leo Cazenille
 */
public final class ClauseImpl extends ForwardingSet<Literal> implements Clause {
	private static final long serialVersionUID = 745838945946L;

	/* --- Properties --- */
	/** The delegate data structure object */
	private Set<Literal> delegate;
	
	/** The symbol that represents the current clause */
	private String label = "";
	
	/** Indicates if the current clause is satisfiable */
	private boolean isSatisfiable = true;

	/** Indicates if the current clause is a tautology */
	private boolean isTautology = false;

	/** Map of annotations of the current clause */
	private final Map<String, Annotation<?, ?>> annotations = new HashMap<String, Annotation<?, ?>>();
	
	/** The resolve strategy to be used by the current clause */
	private transient ResolveStrategy<Clause, Literal> resolveStrategy;
	
	/** The subsumption strategy to be used by the current clause */
	private transient SubsumesStrategy<Clause, Clause> subsumesStrategy;
	
	/** The distribution strategy to be used by the current clause */
	private transient DistributionStrategy<Clause, Literal> distributionStrategy;
	
	/** The union strategy to be used by the current clause */
	private transient UnionStrategy<Clause, Literal> unionStrategy;

	ClauseImpl() {
		this.delegate = new LinkedHashSet<Literal>();
		this.resolveStrategy = ClauseElementsFactory.getInstance().getResolveStrategy();
		this.subsumesStrategy = ClauseElementsFactory.getInstance().getSubsumesStrategy();
		this.distributionStrategy = ClauseElementsFactory.getInstance().getDistributionStrategy();
		this.unionStrategy = ClauseElementsFactory.getInstance().getUnionStrategy();
	}
	
	ClauseImpl(Clause clause) {
		this();
		this.addAll(clause);
		this.isSatisfiable = clause.isSat();
		this.isTautology = clause.isTautology();
		
		for (Entry<String, Annotation<?, ?>> entry : clause.getAnnotations().entrySet()) {
			this.annotations.put(entry.getKey(), entry.getValue().copy());
		}
	}
	
	ClauseImpl(String label, Clause clause) {
		this(clause);
		this.label = label;
	}
	
	ClauseImpl(String label, Collection<Literal> literals) {
		this();
		this.label = label;
		this.addAll(literals);
	}

	/* --- Accessors --- */
	@Override
	public String getLabel() {
		return label;
	}
	
	@Override
	public Map<String, Annotation<? ,?>> getAnnotations() {
		return this.annotations;
	}

	@Override
	public boolean isTautology() {
		return isTautology;
	}

	@Override
	public boolean isSat() {
		return isSatisfiable;
	}

	/* --- Mutators --- */
	@Override
	public void addAnnotation(Annotation<?, ?> annotation) {
		if (this.annotations.containsKey(annotation.getName())) {
			Annotation<?, ?> oldAnnotation = this.annotations.get(annotation.getName());
			oldAnnotation.addContent(annotation.getContent());
		} else {
			this.annotations.put(annotation.getName(), annotation);
		}
	}

	@Override
	public void setTautology(boolean isTautology) {
		this.isTautology = isTautology;
	}

	@Override
	public void setSat(boolean isSat) {
		this.isSatisfiable = isSat;
	}
	
	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Clause copy() {
		return new ClauseImpl(this);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Clause canonicalCopy() {
		Clause result = new ClauseImpl(this);
		result.getAnnotations().clear();
		
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		res.append("(");

		for (Literal l : this) {
			res.append(l);
			res.append(" v ");
		}

		res.delete(res.length() - 3, res.length());
		res.append(")");

		return res.toString();
	}

	/**
	 * Generates a XML representation string of the object
	 * 
	 * @return XML representation string
	 */
	public String toXml() {
		StringBuilder res = new StringBuilder();
		res.append("<clause>");

		for (Literal l : this) {
			res.append(l);
			res.append(" ");
		}

		res.append("</clause>");

		return res.toString();
	}

	/**
	 * Add a literal to the clause, but make sure that there are not two same
	 * literals, or one literal and its negation
	 * 
	 * @return if the literal was added or not
	 */
	@Override
	public boolean add(Literal e) {
		Literal ne = e.opposite();

		if (this.contains(ne) || e instanceof TrueLiteral || this.isTautology()) {
			clear();
			this.delegate.add(new TrueLiteral());
			this.isTautology = true;

			return false;
		} else if (e instanceof FalseLiteral || !this.isSat()) {
			clear();
			this.delegate.add(new FalseLiteral());
			this.isSatisfiable = false;

			return false;
		}

		Literal newLiteral = e.copy();

		return this.delegate.add(newLiteral);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean addAll(Collection<? extends Literal> c) {
		boolean result = false;
		
		for (Literal lit : c) {
			result |= this.add(lit);
		}
		
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Clause union(Clause clause) {
		return this.unionStrategy.performUnion(this, clause);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subsumes(Clause formula) {
		return this.subsumesStrategy.performSubsumes(this, formula);
	}

	/**
	 * @throws UnsupportedOperationException
	 */
	@Override
	public Clause distribution(Clause set) {
		return this.distributionStrategy.performDistribution(this, set);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Clause resolve(Clause formula, Literal literal) {
		return this.resolveStrategy.performResolve(this, formula, literal);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public Set<Literal> delegate() {
		return delegate;
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public void sort(Comparator<Literal> comparator) {
		
		// Verifies if the clause is already ordered following the argument comparator
		if (Ordering.from(comparator).isOrdered(this.delegate))
			return;

		List<Literal> sorteDelegate = Ordering.from(comparator).sortedCopy(this.delegate);
		this.delegate = new LinkedHashSet<Literal>(sorteDelegate);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + annotations.hashCode();
		result = prime * result + (isSatisfiable ? 1231 : 1237);
		result = prime * result + (isTautology ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object o) {
        if (o == this)
            return true;

        if (!(o instanceof Set<?>))
            return false;
        
        if (o instanceof ClauseImpl) {
        	ClauseImpl c = (ClauseImpl) o;
        	
        	return delegate.equals(c.delegate()) && 
        			annotations.equals(c.getAnnotations()) &&
        			isSatisfiable == c.isSat() && isTautology == c.isTautology();
        }
        
        return false;
	}

	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeUTF(label);
		out.writeObject(delegate);
		out.writeBoolean(isSatisfiable);
		out.writeBoolean(isTautology);
		out.writeObject(annotations);
	}

	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		
		this.label = in.readUTF();

		Field fDelegate = ClauseImpl.class.getDeclaredField("delegate");
		fDelegate.set(this, in.readObject());
		
		this.isSatisfiable = in.readBoolean();
		this.isTautology = in.readBoolean();

		Field fAnnotations = ClauseImpl.class.getDeclaredField("annotations");
		fAnnotations.setAccessible(true);
		fAnnotations.set(this, in.readObject());
		
		this.distributionStrategy = ClauseElementsFactory.getInstance().getDistributionStrategy();
		this.resolveStrategy = ClauseElementsFactory.getInstance().getResolveStrategy();
		this.subsumesStrategy = ClauseElementsFactory.getInstance().getSubsumesStrategy();
		this.unionStrategy = ClauseElementsFactory.getInstance().getUnionStrategy();
	}
	
	/**
	 * Compare literals by its symbol, in natural order.
	 * @author Andre Fonseca
	 *
	 */
	public static class SymbolComparator implements Comparator<Literal>, Serializable {
		private static final long serialVersionUID = -195782676888640657L;

		/**
		 * Compares two literals. The one with the smallest symbol in natural order will precedes the biggest symbol.
		 * If the literals has the same symbols, the negative one comes first.
		 * @param l1 : the first literal
		 * @param l2 : the second literal
		 */
		@Override
		public int compare(Literal l1, Literal l2) {
			int result = l1.getLabel().compareToIgnoreCase(l2.getLabel());
			
			if (result == 0) {
				if (!l1.getSign() && l2.getSign()) return 1;
				if (l1.getSign() && !l2.getSign()) return -1;
			}
			 		
			return result;
		}
		
	}

}
