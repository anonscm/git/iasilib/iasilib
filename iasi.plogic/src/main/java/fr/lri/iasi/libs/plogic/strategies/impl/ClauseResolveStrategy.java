package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.FalseLiteral;
import fr.lri.iasi.libs.plogic.strategies.ResolveStrategy;

/**
 * Describes a clause resolving operation.
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseResolveStrategy implements ResolveStrategy<Clause, Literal> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Clause performResolve(Clause formula, Clause otherFormula, Literal literal) {
		// Do not resolve with tautologies...
		if (otherFormula.isTautology() || !otherFormula.isSat())
			return null;

		Clause current = formula.copy();
		Clause formulaTmp = otherFormula.copy();

		// Remove literal and its complementary form the clauses
		boolean res = current.remove(literal);
		if (res) {
			boolean resNeg = formulaTmp.remove(literal.opposite());
			if (!resNeg)
				return null;
		} else {
			boolean resNeg = current.remove(literal.opposite());
			resNeg &= formulaTmp.remove(literal);
			if (!resNeg)
				return null;
		}

		// If there are no more literals left after the resolution, then we have
		// found a contradiction
		if (current.isEmpty() && formulaTmp.isEmpty()) {
			current.add(new FalseLiteral());
			return current;
		}

		// Make the union between the clauses
		return current.union(formulaTmp);
	}

}
