package fr.lri.iasi.libs.plogic.structures;

import java.util.Comparator;
import java.util.Set;

import fr.lri.iasi.libs.plogic.Literal;

/**
 * Trie structure interface. Extends the 'Set' interface and must define 
 * the comparator to be used between its elements.
 * 
 * @author Andre Fonseca
 *
 * @param <T> the type of the elements contained on the trie
 */
public interface Trie<T> extends Set<T> {
	
	/** @return the comparator used by the trie */
	Comparator<Literal> getComparator();
	
	/**
	 * Set the comparator to be used by the trie
	 * 
	 * @param comparator : the comparator to be used
	 */
	void setComparator(Comparator<Literal> comparator);

}
