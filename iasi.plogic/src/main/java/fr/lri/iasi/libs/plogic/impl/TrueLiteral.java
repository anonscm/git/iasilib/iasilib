package fr.lri.iasi.libs.plogic.impl;


/**
 * Describes a true literal
 * @author Andre Fonseca
 * @author Leo Cazenille
 */
public class TrueLiteral extends LiteralImpl {
    private static final long serialVersionUID = 845838945945L;

    public TrueLiteral() {
        super("Tautology");
    }
    
    /**
     * The TrueLiteral is always satisfiable.
     */
    @Override
   	public boolean isSat() {
   		return true;
   	}
    
    /**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canBeDiscarded() {
		return false;
	}
    
    /**
     * The TrueLiteral is always a tautology.
     */
    @Override
	public boolean isTautology() {
		return true;
	}
    
    @Override
	public TrueLiteral copy() {
		return new TrueLiteral();
	}
}
