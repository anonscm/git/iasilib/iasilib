package fr.lri.iasi.libs.plogic;

import java.io.Serializable;

/**
 * Represents a support that may indicate the historic of formulas used to build another formula.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the current support.
 */
public interface Support<T> extends Serializable {

	/**
	* Returns true if the current support subsumes the given support.
	*
	* @param support
	* @return true if the current support subsumes the given support.
	*/
	boolean subsumes(T support);
	
	/**
	* Runs a union operation over the current support and the given one.
	*
	* @param support
	* @return support obtained from the union.
	*/
	T union(T support);
	
	/**
	* @return a copy of the current support.
	*/
	T copy();
}
