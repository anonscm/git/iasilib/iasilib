package fr.lri.iasi.libs.plogic.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.plogic.Literal;

/**
 * Describes a propositional literal
 * @author Andre Fonseca
 * @author Leo Cazenille
 */
public class LiteralImpl implements Literal {
    private static final long serialVersionUID = 745838945945L;

    /* --- Properties --- */
    /** The symbol that represents the current literal */
    protected final String label;
    
    /** The sign of the current literal */
    protected final boolean sign;
    
    /** The annotations map of the current literal */
    protected final Map<String, Annotation<?, ?>> annotations = new HashMap<String, Annotation<?, ?>>();

    public LiteralImpl(String label, boolean sign) {
        if (label == null) {
            throw new IllegalArgumentException("label");
        }

        this.label = label;
        this.sign = sign;
    }
    
    public LiteralImpl(String label, boolean sign, Map<String, Annotation<?, ?>> annotations) {
    	this(label, sign);
    	this.annotations.putAll(annotations);
    }

    public LiteralImpl(String name) {
        this(name, true);
    }

    protected LiteralImpl() {
        this("NamelessClause");
    }

    public LiteralImpl(Literal literal, boolean sign) {
        if (literal == null) {
            throw new IllegalArgumentException("literal");
        }

        this.label = literal.getLabel();
        this.sign = sign;
        this.annotations.putAll(literal.getAnnotations());
    }

    /* --- Accessors --- */
    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public boolean getSign() {
        return sign;
    }
    
    @Override
	public boolean isTautology() {
		return false;
	}

	@Override
	public boolean isSat() {
		return true;
	}

	@Override
	public Map<String, Annotation<?, ?>> getAnnotations() {
		return this.annotations;
	}
	
	/* --- Mutators --- */
	@Override
	public void addAnnotation(Annotation<?, ?> annotation) {
		if (this.annotations.containsKey(annotation.getName())) {
			Annotation<?, ?> oldAnnotation = this.annotations.get(annotation.getName());
			oldAnnotation.addContent(oldAnnotation.getContent());
		} else {
			this.annotations.put(annotation.getName(), annotation);
		}
	}
	
	/* --- Methods --- */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canBeDiscarded() {
		return true;
	}
	
	@Override
    public boolean equals(Object o) {
		if (o == null)
			return false;
		
        if (o.getClass() != this.getClass()) {
        	return false;
        }
        
        Literal l = (LiteralImpl) o;

        return ((this.label.equals(l.getLabel())) && (this.sign == l.getSign()));
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + (sign ? 1231 : 1237);
		return result;
	}
	
    @Override
    public String toString() {
        String signStr = "";

        if (!sign) {
            signStr = "!";
        }

        return signStr + label;
    }
    
    /**
     * Generates a XML representation string of the object
     * @return XML representation string
     */
    public String toXml() {
        String signStr = "";

        if (!sign) {
            signStr = "</not>";
        }

        return "<literal>" + signStr + label + "</literal>";
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
    public Literal copy() {
        return new LiteralImpl(label, sign);
    }
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Literal canonicalCopy() {
		Literal result = new LiteralImpl(label, sign);
		result.getAnnotations().clear();
		
		return result;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Literal opposite() {
		return new LiteralImpl(this, !sign);
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeUTF(label);
		out.writeBoolean(sign);
		out.writeObject(annotations);
	}
	
	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		
		Field flabel = LiteralImpl.class.getDeclaredField("label");
		flabel.setAccessible(true);
		flabel.set(this, in.readUTF());
		
		Field fSign = LiteralImpl.class.getDeclaredField("sign");
		fSign.setAccessible(true);
		fSign.set(this, in.readBoolean());
		
		Field fAnnotations = LiteralImpl.class.getDeclaredField("annotations");
		fAnnotations.setAccessible(true);
		fAnnotations.set(this, in.readObject());
	}

}
