package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Ordering;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.structures.ClauseTrie;
import fr.lri.iasi.libs.plogic.structures.ClauseTrieNode;

/**
 * Subsumption operation on a clause conjunction (trie).
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseTrieSubsumesStrategy extends ClauseCollectionSubsumesStrategy<ConjunctionOfClauses> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performSubsumes(ConjunctionOfClauses formula, Clause otherFormula) {
		if (otherFormula.isTautology() || !formula.isSat())
			return true;
		
		if (!otherFormula.isSat())
			return false;
		
		Comparator<Literal> comparator = ((ClauseTrie)formula.delegate()).getComparator();
		otherFormula.sort(comparator);
		
		return subsumesHelper(otherFormula, ((ClauseTrie)formula.delegate()).getRoot(), comparator);
	}
	
	/**
	 * Helper function to compute subsequent steps of the subsumes operation.
	 * @param remaingLiterals of the verified clause
	 * @param node the current node on the recursion
	 * @param the comparator being used by the delegate trie
	 * @return true if the clause can be subsumed by the current cnf 
	 */
	private boolean subsumesHelper(Clause remaingClause, ClauseTrieNode node, Comparator<Literal> comparator) {
		if (node.isLeaf())
			return true;
		
		// Assures the order of the children list
		List<ClauseTrieNode> children = node.getChildren();
		if (!Ordering.natural().isOrdered(children))
			Collections.sort(children);
		
		Iterator<ClauseTrieNode> it = children.iterator();
		while (it.hasNext()) {
			ClauseTrieNode child = it.next();
			Clause remaingCopy = remaingClause.copy();

			Iterator<Literal> itRemaing = remaingCopy.iterator();
			while (itRemaing.hasNext()) {
				Literal lit = itRemaing.next();
				
				if (comparator.compare(lit, child.getLabel()) < 0)
					itRemaing.remove();
				else
					break;
			}
			
			if (remaingCopy.isEmpty())
				return false;
			
			Iterator<Literal> itLit = remaingCopy.iterator();
			Literal first = itLit.next();
			
			if (child.getLabel().equals(first)) {
				remaingCopy.remove(first);
				if (subsumesHelper(remaingCopy, child, comparator))
					return true;
			}
		}
		
		return false;
	}

}
