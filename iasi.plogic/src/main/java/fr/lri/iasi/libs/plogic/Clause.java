package fr.lri.iasi.libs.plogic;

import java.util.Comparator;

import fr.lri.iasi.libs.formallogic.types.DisjunctionFormula;

/**
 * The clause element representation.
 * 
 * @author Andre Fonseca
 *
 */
public interface Clause extends DisjunctionFormula<Clause, Literal> {
	
	/**
	* Resolve between the current formula and the given one, on the given literal (that belongs to the current formula).
	*
	* @param formula is the given formula
	* @param onElement is the element to resolve
	* @return Formula is the resolution between the current formula and the given one, on the given element.
	*/
	Clause resolve(Clause formula, Literal onElement);
	
	/**
	* Returns true if the current formula subsumes the given formula.
	*
	* @param formula
	* @return true if the current formula subsumes the given formula.
	*/
	boolean subsumes(Clause clause);
	
	/**
	 * In-place sorting of clause.
	 * 
	 * @param literals comparator to be used
	 */
	void sort(Comparator<Literal> comparator);

}
