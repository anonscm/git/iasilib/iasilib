package fr.lri.iasi.libs.plogic.restrictions;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;

/**
 * Describes the empty clause restriction.
 * 
 * @author Andre Fonseca
 *
 */
public class EmptyClauseRestriction implements Restriction<Clause> {
	private static final long serialVersionUID = 663142610453180862L;

	/* --- Accessors --- */
	@Override
	public String getName() {
		return "empty";
	}

	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean discard(Clause clause) {
		if (!clause.isSat())
			return false;
		
		return true;
	}
	
	@Override
	public String toString() {
        return "Empty clause";
    }

}
