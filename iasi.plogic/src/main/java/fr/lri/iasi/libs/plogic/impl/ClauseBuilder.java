package fr.lri.iasi.libs.plogic.impl;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.formallogic.FormulaBuilder;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;

/**
 * Implementation of the Builder pattern to generate immutable clauses.
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseBuilder implements FormulaBuilder<Clause> {
	
	/* --- Properties --- */
	/** The id of the clause to be generated */
	protected String clauseLabel;
	
	/** The literals contained on the immutable clause to be generated */
	protected final Set<Literal> literals;
	
	/** The annotations contained on the immutable clause to be generated */
	protected final Map<String, Annotation<?,?>> annotations;

	public ClauseBuilder() {
		this.clauseLabel = "";
		this.literals = new LinkedHashSet<Literal>();
		this.annotations = new LinkedHashMap<String, Annotation<?,?>>();
	}

	/* --- Methods --- */
	/**
	 * Set the label of the clause to be generated.
	 * @param symbol : the label of the clause to be generated
	 */
	public void setLabel(String label) {
		this.clauseLabel = label;
	}
	
	/**
	 * Add annotation to the clause to be generated
	 * @param annotation
	 */
	public void addAnnotation(Annotation<?, ?> annotation) {
		this.annotations.put(annotation.getName(), annotation);
	}
	
	/**
	 * Add a collection of annotations to the clause to be generated
	 * @param annotation
	 */
	public void addAllAnnotations(Collection<Annotation<?, ?>> annotations) {
		for (Annotation<?, ?> annotation : annotations) {
			this.addAnnotation(annotation);
		}
	}
	
	/**
	 * Create and add a literal to the clause to be generated. The literal will be positive its name will be based on the symbol parameter.
	 * @param symbol the name of the literal
	 */
	public void addLiteral(String symbol) {
		if (verifySpecialElements(symbol))
			return;
		
		Literal lit = new LiteralImpl(symbol, true);
		this.literals.add(lit);
	}
	
	/**
	 * Add an existing literal to the clause to be generated.
	 * @param symbol the name of the literal
	 */
	public void addLiteral(Literal literal) {
		
		Literal lit = literal.copy();
		this.literals.add(lit);
	}

	/**
	 * Create and add a literal to the clause to be generated. The literal will be negative its name will be based on the symbol parameter.
	 * @param symbol the name of the literal
	 */
	public void addNegativeLiteral(String symbol) {
		if (verifySpecialElements(symbol))
			return;
		
		Literal lit = new LiteralImpl(symbol, false);
		this.literals.add(lit);
	}
	
	/**
	 * Add a {@link FalseLiteral} to the clause to be generated if the symbol is "empty". If the symbol is "true" a {@link TrueLiteral}
	 * will be add.
	 * @param symbol the name of the literal
	 */
	protected boolean verifySpecialElements(String symbol) {
		if (symbol.equalsIgnoreCase("empty")) {
			this.literals.add(new FalseLiteral());
			return true;
		}
		
		if (symbol.equalsIgnoreCase("true")) {
			this.literals.add(new TrueLiteral());
			return true;
		}
		
		return false;
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public Clause create() {
		Clause clause = new ClauseImpl(this.clauseLabel, this.literals);
		
		Set<Annotation<?, ?>> annotations = ClauseElementsFactory.getInstance().getAnnotations();
		for (Annotation<?, ?> annotation : annotations) {
			if (!this.annotations.containsKey(annotation.getName()))
				clause.addAnnotation(annotation.copy());
			else
				clause.addAnnotation(this.annotations.get(annotation.getName()).copy());
		}
		
		return clause;
	}

}
