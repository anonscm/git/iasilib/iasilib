package fr.lri.iasi.libs.plogic.structures;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

import java.util.Comparator;
import java.util.List;

import com.google.common.collect.Lists;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;

/**
 * Describes a node of a clause trie. Each node represents a literal and, if it is a leaf,
 * it will also represent a clause formed by the path between the root node and itself.
 *  
 * @author Andre Fonseca
 *
 */
public class ClauseTrieNode implements Comparable<ClauseTrieNode>, Serializable {
	private static final long serialVersionUID = -3902256532194673221L;

	/* --- Properties --- */
	/** A reference for the container trie */
	private final ClauseTrie trie;
	
	/** The parent of the current node */
	private ClauseTrieNode parent;
 
	/** The literal represented by the current node */
    private Literal label;
    
    /** If the current node is a leaf, it will also represent a clause */
    private Clause leafValue;
 
    /** The set of nodes representing the children of the current node */
    private List<ClauseTrieNode> children = Lists.newArrayList();
 
    public ClauseTrieNode(final ClauseTrie trie, final Literal value) {
    	if (trie == null) {
    		throw new IllegalArgumentException("trie");
    	}
    	
    	this.trie = trie;
        this.label = value;
    }
    
    /* --- Accessors --- */
    public Literal getLabel() {
        return label;
    }
    
    public Clause getLeafValue() {
    	return leafValue;
    }
    
    public ClauseTrieNode getParent() {
        return parent;
    }
    
    public List<ClauseTrieNode> getChildren() {
        return children;
    }
    
    /* --- Mutators --- */
    public void setParent(ClauseTrieNode node) {
        this.parent = node;
    }
    
    public void setLeafValue(Clause clause) {
        this.leafValue = clause;
    }
 
    /* --- Methods --- */
    /**
     * Returns the children node representing the target literal.
     * @param value the literal which the child node must correspond
     * @return the child node corresponding to the literal value, null if it does not exist.
     */
    public ClauseTrieNode findChild(Literal value) {
        for (ClauseTrieNode n : children) {
            if (n.getLabel().equals(value))
                return n;
        }
            
        return null;
    }
    
    /**
     * @return a list of leafs from the current node
     */
	public List<Clause> findLeaves() {
    	List<Clause> result = Lists.newArrayList();
    	
    	if (this.leafValue != null)
    		result.add(this.leafValue);
    	
    	for (ClauseTrieNode n : this.children) {
    		result.addAll(n.findLeaves());
        }
    	
		return result;
    }
    
    /**
     * Indicates if the current node is a leaf
     * @return true if the current node is a leaf, false otherwise
     */
    public boolean isLeaf() {
    	if (this.leafValue != null)
    		return true;
    	
    	return false;
    }
 
    /**
     * Creates and add a child node to the current node
     * @param value the literal that will correspond to the child node
     * @return the new node
     */
    public ClauseTrieNode addChild(Literal value) {
    	ClauseTrieNode node = new ClauseTrieNode(this.trie, value);
    	node.setParent(this);
        children.add(node);
        
        return node;
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if (leafValue != null) {
			result = prime * result + leafValue.hashCode();
		} else {
			result = prime * result + ((label == null) ? 0 : label.hashCode());
			result = prime * result + ((parent == null) ? 0 : parent.hashCode());
			result = prime * result + ((children == null) ? 0 : children.hashCode());
		}
		return result;
	}
	
	@Override
    public boolean equals(Object o) {
    	if (this == o)
    		return true;
    	
    	if (o == null)
			return false;
    	
    	if (o instanceof ClauseTrieNode) {
    		ClauseTrieNode node = (ClauseTrieNode) o;
    		
	    	if (leafValue != null && node.getLeafValue() != null)
	    		return leafValue.equals(node.getLeafValue());
	    	else if (leafValue == null && node.getLeafValue() == null)
	    		return (parent != null && node.getParent() != null) ? 
	    			label.equals(node.getLabel()) && parent.equals(node.getParent()) && children.equals(node.getChildren()): 
	    			label.equals(node.getLabel()) && children.equals(node.getChildren()) && parent == null && node.getParent() == null;
    	}
    	
    	return false;
    }

	@Override
	public int compareTo(ClauseTrieNode node) {
		Comparator<Literal> comparator = this.trie.getComparator();
		return comparator.compare(this.getLabel(), node.getLabel());
	}
	
	@Override
    public String toString() {
        String res = "Label: " + label;
        res += "\nLeafValue: " + leafValue + "\n";

        return res;
    }
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(trie);
		out.writeObject(parent);
		out.writeObject(label);
		out.writeObject(leafValue);
		out.writeObject(children);
	}
	
	/**
	 * Manual deserialization
	 */
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		
		Field fTrie = ClauseTrieNode.class.getDeclaredField("trie");
		fTrie.setAccessible(true);
		fTrie.set(this, in.readObject());
		
		this.parent = (ClauseTrieNode) in.readObject();
		this.label = (Literal) in.readObject();
		this.leafValue = (Clause) in.readObject();
		
		Field fChildren = ClauseTrieNode.class.getDeclaredField("children");
		fChildren.setAccessible(true);
		fChildren.set(this, in.readObject());
	}
 
}
