package fr.lri.iasi.libs.plogic.structures;

import java.util.Collection;

/**
 * Represents a trie node. If the node is a leaf, it must also contains
 * the object represented by the path from the root until it.
 * 
 * @author Andre Fonseca
 *
 * @param <T> the node element type (recursive)
 * @param <V> the type of the element contained in the leaf.
 * @param <S> the type of element contained on each node. The union of the elements on each node will build the element represented on the leaf.
 */
public interface TrieNode<T extends TrieNode<T, V, S>, V, S> {
	
	/**
	 * Adds a child node to the current node.
	 * 
	 * @param value : the content of the child node.
	 * @return the new node.
	 */
	T addChild(S value);
	
	/** Finds the child node containing the given value
	 * 
	 * @param value : the content of the child node to be retrieved.
	 * @return the target node or <b>null</b> if the node does not exist.
	 */
	T findChild(S value);
	
	/**
	 * Finds all leaves values related with the current node.
	 * 
	 * @return the collection of leaves values.
	 */
	Collection<V> findLeaves();
	
	/**
	 * Indicates if the current node is a leaf.
	 * 
	 * @return true if the current node is a leaf, false otherwise.
	 */
	boolean isLeaf();

}
