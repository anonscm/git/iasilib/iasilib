package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;
import fr.lri.iasi.libs.plogic.UnionStrategy;

/**
 * Default implementation of the union strategy.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formulas to be united 
 * @param <F> The type of the formula contained on the formulas to be united.
 */
public class DefaultUnionStrategy<T extends NaryOperatorFormula<T, F>, F> implements UnionStrategy<T, F> {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public T performUnion(T formula, T otherFormula) {
		if (!formula.isSat())
			return otherFormula;

		if (!otherFormula.isSat())
			return formula;
		
		T result = formula.copy();
		result.addAll(otherFormula);
		
		return result;
	}

}
