package fr.lri.iasi.libs.plogic;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;

/**
 * Strategy that describes to distribution of two formulas, with the same type.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formula to be distributed
 * @param <F> The type of the formula contained on the formulas to be distributed
 */
public interface DistributionStrategy<T extends NaryOperatorFormula<T, F>, F> {
	
	/**
	 * Performs the distribution operation on two formulas.
	 * 
	 * @param formula
	 * @param otherFormula
	 * @return a new formula representing the distribution of the two arg clauses.
	 */
	T performDistribution(T formula, T otherFormula);

}
