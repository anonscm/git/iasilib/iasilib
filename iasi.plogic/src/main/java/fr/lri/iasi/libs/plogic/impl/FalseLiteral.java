package fr.lri.iasi.libs.plogic.impl;

/**
 * Representation of the "Empty" literal.
 * @author Andre Fonseca
 *
 */
public class FalseLiteral extends LiteralImpl {
    private static final long serialVersionUID = 845838945945L;

    public FalseLiteral() {
        super("Empty");
    }
    
    /**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canBeDiscarded() {
		return false;
	}
    
    /**
     * The FalseLiteral is always unsatisfiable.
     */
    @Override
	public boolean isSat() {
		return false;
	}
    
    /**
     * The FalseLiteral is never a tautology.
     */
    @Override
	public boolean isTautology() {
		return false;
	}
    
    @Override
	public FalseLiteral copy() {
		return new FalseLiteral();
	}
}
