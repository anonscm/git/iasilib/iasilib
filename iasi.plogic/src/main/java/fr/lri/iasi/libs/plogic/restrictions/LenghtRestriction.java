package fr.lri.iasi.libs.plogic.restrictions;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;

/**
 * Describes a clause restriction by size.
 * 
 * @author Andre Fonseca
 *
 */
public class LenghtRestriction implements Restriction<Clause> {
	private static final long serialVersionUID = 3078350945534446709L;
	
	/* --- Properties --- */
	/** Max length of clauses to be allowed */
	private final int maxLenght;
	
	public LenghtRestriction(int maxLenght) {
		if (maxLenght < 0) {
			throw new IllegalArgumentException("lenght");
		}
		
		this.maxLenght = maxLenght;
	}

	/* --- Accessors --- */
	@Override
	public String getName() {
		return "lenght";
	}

	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean discard(Clause clause) {
		if (clause.isSat())
			return false;
		
		if (clause.size() > maxLenght)
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
        return "Bounded length: " + maxLenght;
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + maxLenght;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (!(obj instanceof LenghtRestriction))
			return false;
		
		LenghtRestriction other = (LenghtRestriction) obj;
		if (maxLenght != other.maxLenght)
			return false;
		
		return true;
	}

}
