package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;
import fr.lri.iasi.libs.plogic.DistributionStrategy;

/**
 * Default implementation of the distribution strategy.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formulas to be distributed.
 * @param <F> The type of the formula contained on the formulas to be distributed.
 */
public class DefaultDistributionStrategy<T extends NaryOperatorFormula<T, F>, F extends NaryOperatorFormula<F, ?>> 
										implements DistributionStrategy<T, F> {
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T performDistribution(T formula, T otherformula) {
		T result = null;
		F tmp = null; 
		
		try {
			result = (T) formula.getClass().newInstance();
		} catch (InstantiationException e) {
			return null;
		} catch (IllegalAccessException e) {
			return null;
		}     

        for (F f1 : formula) {
            for (F f2 : otherformula) {
                tmp = f1.union(f2);

                if (tmp != null)
                	result.add(tmp);
            }
        }

        return result;
	}

}
