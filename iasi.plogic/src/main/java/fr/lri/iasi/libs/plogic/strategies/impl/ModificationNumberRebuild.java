package fr.lri.iasi.libs.plogic.strategies.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesImpl;
import fr.lri.iasi.libs.plogic.structures.Trie;
import fr.lri.iasi.libs.plogic.structures.TrieRebuildStrategy;

/**
 * A theory rebuild strategy based on a limited number of modifications.
 * 
 * @author Andre Fonseca
 *
 */
public class ModificationNumberRebuild implements TrieRebuildStrategy<Clause> {
	private static final long serialVersionUID = 5269131283175487869L;

	/* --- Properties --- */
	/** The number of allowed modifications in a theory */
	private int modLimit = 10000;
	
	/** The frequency map that indicates the frequency of literals on a set to be used during the trie rebuild */
    private final Multiset<Literal> literalFrequencyMap = HashMultiset.create();
    
    /** The frequency map that indicates the frequency of literals on the last trie rebuild */
    private Multiset<Literal> lastLiteralFrequencyMap;
    
    /** Comparator to be used to reorder clauses before inserting then into the theory (may be null) */
    private Comparator<Literal> clauseReorderComparator;
    
    /** The trie to be rebuilt */
    private Trie<Clause> trie;
    
    /** The trie modification counter */
	private final AtomicInteger modCount = new AtomicInteger();
	
	/** Flag indicating if new modifications can be counted */
	private final AtomicBoolean canCountModifications = new AtomicBoolean();

	public ModificationNumberRebuild(Trie<Clause> trie) {
		if (trie == null) {
			throw new IllegalArgumentException("theory");
		}
		
		this.trie = trie;
		this.clauseReorderComparator = new LiteralFrequencyComparator();
		this.modCount.set(0);
		this.canCountModifications.set(true);
	}
	
	public ModificationNumberRebuild(Trie<Clause> trie, int modLimit) {
		this(trie);
		this.modLimit = modLimit;
	}
	
	public ModificationNumberRebuild(Trie<Clause> trie, Comparator<Literal> clauseReorderComparator) {
		this(trie);
		this.clauseReorderComparator = clauseReorderComparator;
	}
	
	public ModificationNumberRebuild(Trie<Clause> trie, Comparator<Literal> clauseReorderComparator, int modLimit) {
		this(trie, clauseReorderComparator);
		this.modLimit = modLimit;
	}
	
	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public void analyzeModification(boolean isAdding, Clause... params) {
		if (canCountModifications.get()) {	
		
			for (Clause clause : params) {
				// Increments or decrements the literal counter on the multiset
				for (Literal literal : clause) {
					if (isAdding)
						this.literalFrequencyMap.add(literal);
					else
						this.literalFrequencyMap.remove(literal);
				}
				
				// If the number of modifications passed the modification limit...
				if (this.modCount.incrementAndGet() >= this.modLimit) {
					
					// Copies the literal frequency map to be used as a reference until the next rebuild
					this.lastLiteralFrequencyMap = HashMultiset.create(this.literalFrequencyMap);
					
					// Rebuild trie
					rebuild();
				}
			}
		}
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void rebuild() {
		// Copies the clauses contained on the trie.
		List<Clause> clauses = new LinkedList<Clause>();
		Iterator<Clause> itCopy = this.trie.iterator();
		while (itCopy.hasNext()) {
			Clause clause = itCopy.next();
			clauses.add(clause.copy());
		}
		
		// Do not count the following modifications
		this.canCountModifications.set(false);
		this.trie.clear();
		this.trie.setComparator(clauseReorderComparator);
		
		// Rebuilding the trie...
		Iterator<Clause> it = clauses.iterator();
		while (it.hasNext()) {
			Clause clause = it.next();
			this.trie.add(clause);
		}
		
		// Needs to count the next modifications
		this.canCountModifications.set(true);
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public void reset() {
		this.modCount.set(0);
		
		if (canCountModifications.get()) {
			this.literalFrequencyMap.clear();
		}
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeInt(modLimit);
		out.writeObject(literalFrequencyMap);
		out.writeObject(lastLiteralFrequencyMap);
		out.writeObject(clauseReorderComparator);
		out.writeObject(trie);
		out.writeObject(modCount);
		out.writeObject(canCountModifications);
	}
	
	/**
	 * Manual deserialization
	 */
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		
		this.modLimit = in.readInt();
		
		Field fliteralFrequencyMap = ConjunctionOfClausesImpl.class.getDeclaredField("literalFrequencyMap");
		fliteralFrequencyMap.setAccessible(true);
		fliteralFrequencyMap.set(this, in.readObject());
		
		this.lastLiteralFrequencyMap = (Multiset<Literal>) in.readObject();
		this.clauseReorderComparator = (Comparator<Literal>)in.readObject();
		this.trie = (Trie<Clause>) in.readObject();
		
		Field fmodCount = ConjunctionOfClausesImpl.class.getDeclaredField("modCount");
		fmodCount.setAccessible(true);
		fmodCount.set(this, in.readObject());
		
		Field fcanCountModifications = ConjunctionOfClausesImpl.class.getDeclaredField("canCountModifications");
		fcanCountModifications.setAccessible(true);
		fcanCountModifications.set(this, in.readObject());
	}
	
	/**
	 * Literals comparator based on its frequency on the current theory.
	 * 
	 * @author Andre Fonseca
	 *
	 */
	public class LiteralFrequencyComparator implements Comparator<Literal>, Serializable {
		private static final long serialVersionUID = 864134619303325570L;

		@Override
		public int compare(Literal l1, Literal l2) {
			if (l1.equals(l2)) return 0;
			
			Integer occurrencyLit1 = Integer.valueOf(lastLiteralFrequencyMap.count(l1));
			Integer occurrencyLit2 = Integer.valueOf(lastLiteralFrequencyMap.count(l2));
			int result = (-occurrencyLit1.compareTo(occurrencyLit2));
			
			if (result == 0) {
				int labelComparation = l1.getLabel().compareToIgnoreCase(l2.getLabel());
				if (labelComparation == 0) {
					if (!l1.getSign() && l2.getSign()) return 1;
					if (l1.getSign() && !l2.getSign()) return -1;
				}
				
				return labelComparation;
			}
			
			return result;
		}
		
	}
}
