package fr.lri.iasi.libs.plogic.util;

/**
 * The pair class represents a tuple that may be set as a method return type.
 * 
 * @author Andre Fonseca
 *
 * @param <U> the type of the first tuple element
 * @param <V> the type of the second tuple element
 */
public class Pair<U, V> {

	/* --- Properties --- */
	/** The first tuple element */
    private U first;

    /** The second tuple element */
    private V second;

    public Pair(U first, V second) {

        this.first = first;
        this.second = second;
    }
    
    /* --- Accessors --- */
    public U getFirst() {
    	return first;
    }
    
    public V getSecond() {
    	return second;
    }
    
}
