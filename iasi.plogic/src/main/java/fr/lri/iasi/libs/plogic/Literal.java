package fr.lri.iasi.libs.plogic;

import fr.lri.iasi.libs.formallogic.AtomicFormula;

/**
 * The literal representation.
 * 
 * @author Andre Fonseca
 *
 */
public interface Literal extends AtomicFormula<Literal> {

	/**
	 * Returns the signal (positive/negative) of the current literal
	 * 
	 * @return the signal of the current literal
	 */
    boolean getSign();
    
    /**
     * @return false if the literal belongs to a special type (true literal, empty literal or other) 
     * and cannot be discarded. Return true otherwise.
     */
    boolean canBeDiscarded();
	
}
