package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.formallogic.NaryOperatorFormula;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;

/**
 * Default implementation of the subsumes operation.
 * 
 * @author Andre Fonseca
 *
 * @param <T> The type of the formulas where the subsumption test will run.
 */
public class DefaultSubsumesStrategy<T extends NaryOperatorFormula<T, ?>> implements SubsumesStrategy<T, T> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performSubsumes(T formula, T otherFormula) {
		if (otherFormula.isTautology() || !formula.isSat())
			return true;
		
		if (!otherFormula.isSat())
			return false;

		if (otherFormula.containsAll(formula))
			return true;

		return false;
	}

}
