package fr.lri.iasi.libs.plogic.structures;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseImpl;
import fr.lri.iasi.libs.plogic.strategies.impl.ModificationNumberRebuild;

/**
 * Describes a compact representation of a set of clauses using a Trie.
 * 
 * @author Andre Fonseca
 * 
 */
public class ClauseTrie implements Trie<Clause>, Serializable {
	private static final long serialVersionUID = -3937392259076125956L;

	/* --- Properties --- */
	/** The root node of the trie */
	private ClauseTrieNode root;

	/** A map between the leaves of the trie and the clause represented by it */
	private Map<Clause, ClauseTrieNode> leaves;
	
	/** The comparator used to sort literals on the trie. Default: SymbolComparator */
	private Comparator<Literal> literalsComparator;
	
	/** The strategy used to rebuild the trie */
	private TrieRebuildStrategy<Clause> rebuildStrategy = new ModificationNumberRebuild(this);

	public ClauseTrie() {
		this.root = new ClauseTrieNode(this, null);
		// It is important to preserve the clause insertion order!
		this.leaves = Maps.newLinkedHashMap();
		this.literalsComparator = new ClauseImpl.SymbolComparator();
	}

	/* --- Accessors --- */
	public ClauseTrieNode getRoot() {
		return root;
	}

	public Map<Clause, ClauseTrieNode> getLeaves() {
		return leaves;
	}
	
	public Comparator<Literal> getComparator() {
		return literalsComparator;
	}
	
	/* --- Mutators --- */
	public void setComparator(Comparator<Literal> comparator) {
		this.literalsComparator = comparator;
	}

	/* --- Methods --- */
	@Override
	public boolean add(Clause clause) {
		if (!clause.isSat())
			this.clear();

		ClauseTrieNode current = root;
		clause.sort(literalsComparator);

		Iterator<Literal> it = clause.iterator();
		while (it.hasNext()) {
			Literal value = it.next();
			ClauseTrieNode child = current.findChild(value);

			if (child != null) {
				current = child;
			} else {
				current = current.addChild(value);

				if (!it.hasNext()) {
					current.setLeafValue(clause);
					leaves.put(clause, current);
					
					rebuildStrategy.analyzeModification(true, clause);
					
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public int size() {
		return leaves.size();
	}

	@Override
	public boolean isEmpty() {
		return leaves.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		if (o instanceof Clause) {
			Clause clause = (Clause) o;
			clause.sort(literalsComparator);
			ClauseTrieNode current = root;

			for (Literal lit : clause) {
				ClauseTrieNode child = current.findChild(lit);
				if (child == null)
					return false;
				else
					current = child;
			}

			return true;
		}

		return false;
	}

	@Override
	public Iterator<Clause> iterator() {
		return new TrieLeavesIterator(leaves.keySet().iterator());
	}

	@Override
	public Object[] toArray() {
		return leaves.keySet().toArray();
	}

	/**
	 * @throws UnsupportedOperationException
	 */
	@Override
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException("Not implemented");
	}

	/**
	 * NOTE: May be used to remove either Clauses or nodes on the Trie.
	 */
	@Override
	public boolean remove(Object o) {
		return remove(o, false);
	}
	
	private boolean remove(Object o, boolean calledByIterator) {
		if (o instanceof ClauseTrieNode) {
			ClauseTrieNode node = (ClauseTrieNode) o;

			// When deleting a single node, all associated leaves (clauses) must be deleted.
			return removeHelper(node, true);
		} else if (o instanceof Clause) {
			Clause clause = (Clause) o;
			ClauseTrieNode current = null;
			
			if (calledByIterator)
				current = this.leaves.get(clause);
			else
				current = this.leaves.remove(clause);

			if (current == null)
				return false;
			
			// Delete every associated node if they are not associated with other clauses.
			while (current.getParent() != null) {
				if (current.isLeaf() || current.getChildren().size() < 1) {
					
					// When deleting a clause, leaves associated with deleted nodes must be spared.
					removeHelper(current, false);
					current = current.getParent();
				} else {
					break;
				}
			}

			return true;
		}

		return false;
	}
	
	
	/**
	 * Remove helper function. It is used when deleting a single node from the trie.
	 * @param node is the current node
	 * @param canDeleteOtherLeaves indicates if associated clauses have to be deleted
	 * @return if the node and its correspondent leaves were correctly removed, false otherwise.
	 */
	private boolean removeHelper(ClauseTrieNode node, boolean canDeleteOtherLeaves) {
		// If the operation can delete associated leaves...
		boolean result = false;
		
		if (canDeleteOtherLeaves) {
			List<Clause> relatedLeaves = node.findLeaves();
			for (Clause clause : relatedLeaves) {
				ClauseTrieNode removed = this.leaves.remove(clause);
				if (removed != null)
					result = true;
			}
		}

		ClauseTrieNode parent = node.getParent();
		if (parent != null) {
			parent.getChildren().remove(node);
			result = true;
		}
		
		return result;
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		boolean modified = false;

		for (Object object : c) {
			modified |= this.remove(object, false);
		}
		
		// Rebuild the trie (if needed) after the remotion of the nodes...
		rebuildStrategy.analyzeModification(false, c.toArray(new Clause[c.size()]));

		return modified;
	}

	@Override
	public void clear() {
		this.root.getChildren().clear();
		this.leaves.clear();
		this.rebuildStrategy.reset();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object object : c) {
			if (!this.contains(object))
				return false;
		}

		return true;
	}

	@Override
	public boolean addAll(Collection<? extends Clause> c) {
		boolean result = false;
		
		for (Clause clause : c) {
			result |= this.add(clause);
		}

		return result;
	}

	/**
	 * @throws UnsupportedOperationException
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;

		if (!(o instanceof Set<?>))
			return false;

		if (o instanceof ClauseTrie) {
			ClauseTrie c = (ClauseTrie) o;
			if (c.getLeaves().size() != leaves.size())
				return false;
	
			return leaves.keySet().equals(c.getLeaves().keySet());
		}
		
		return false;
	}

	@Override
	public int hashCode() {
		int h = 0;
		Iterator<Clause> i = iterator();
		while (i.hasNext()) {
			Clause obj = i.next();
			if (obj != null)
				h += obj.hashCode();
		}
		return h;
	}
	
	/**
	 * Manual serialization of the attributes
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeObject(root);
		out.writeObject(leaves);
		out.writeObject(literalsComparator);
	}
	
	/**
	 * Manual deserialization
	 */
	@SuppressWarnings("unchecked")
	private void readObject(ObjectInputStream in) throws IOException,
			ClassNotFoundException, SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		
		this.root = (ClauseTrieNode)in.readObject();
		this.leaves = (Map<Clause, ClauseTrieNode>)in.readObject();
		this.literalsComparator = (Comparator<Literal>)in.readObject();
	}

	/**
	 * Iterator responsible for extend the remove action to act over the trie
	 * nodes. It is also responsible to assure the iteration only over the leafs
	 * (clauses) and not over each node of the trie.
	 * 
	 * @author Andre Fonseca
	 * 
	 */
	public class TrieLeavesIterator implements Iterator<Clause> {

		protected Iterator<Clause> realIterator;

		protected Clause current;

		public TrieLeavesIterator(Iterator<Clause> delegate) {
			if (delegate == null) {
				throw new IllegalArgumentException("realIterator");
			}

			this.realIterator = delegate;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			return this.realIterator.hasNext();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Clause next() {
			this.current = this.realIterator.next();
			return current;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void remove() {			
			ClauseTrie.this.remove(current, true);
			
			this.realIterator.remove();
		}
	}

	
}
