package fr.lri.iasi.libs.plogic.strategies.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;

/**
 * Clause removal operation on a clause conjunction (hashset).
 * 
 * @author Andre Fonseca
 *
 */
public class ClauseCollectionRemoveFormulaStrategy implements RemoveFormulaStrategy<ConjunctionOfClauses, Clause> {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performRemove(ConjunctionOfClauses collection, Object formula) {
		return collection.delegate().remove(formula);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Clause> performRemoveSubsumed(ConjunctionOfClauses collection, Clause formula) {
		Collection<Clause> result = new LinkedList<Clause>();
		
		Iterator<Clause> it = collection.iterator();
		while (it.hasNext()) {
			Clause clause = it.next();
			if (formula.subsumes(clause)) {
				it.remove();
				result.add(clause);
			}
		}
		
		return result;
	}

}
