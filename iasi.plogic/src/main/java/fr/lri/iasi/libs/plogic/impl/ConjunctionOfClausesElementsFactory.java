package fr.lri.iasi.libs.plogic.impl;

import java.util.LinkedHashSet;
import java.util.Set;

import fr.lri.iasi.libs.plogic.AddFormulaStrategy;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.DistributionStrategy;
import fr.lri.iasi.libs.plogic.UnionStrategy;
import fr.lri.iasi.libs.plogic.strategies.RemoveFormulaStrategy;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;
import fr.lri.iasi.libs.plogic.structures.ClauseTrie;

/**
 * Strategy factory. Responsible for the instantiation of the main strategies and
 * default annotations used by a conjunction of clauses.
 * 
 * @author Andre Fonseca
 *
 */
public final class ConjunctionOfClausesElementsFactory {

	/* --- Properties --- */
	/** The singleton object */
	private static ConjunctionOfClausesElementsFactory instance = new ConjunctionOfClausesElementsFactory();
	
	/** The union strategy to be used by all conjunction of clauses */
	private UnionStrategy<ConjunctionOfClauses, Clause> unionStrategyToBeUsed;
	
	/** The subsumption strategy to be used by all conjunction of clauses */
	private SubsumesStrategy<ConjunctionOfClauses, Clause> subsumesStrategyToBeUsed;
	
	/** The distribution strategy to be used by all conjunction of clauses */
	private DistributionStrategy<ConjunctionOfClauses, Clause> distributionStrategyToBeUsed;
	
	/** The add clauses strategy to be used by all conjunction of clauses */
	private AddFormulaStrategy<ConjunctionOfClauses, Clause> addFormulaStrategy;
    
	/** The remove clauses strategy to be used by all conjunction of clauses */
	private RemoveFormulaStrategy<ConjunctionOfClauses, Clause> removeFormulaStrategy;
	
	private ConjunctionOfClausesElementsFactory() {
		StrategiesConfigurator.getInstance().configureStrategies(this);
	}
	
	public static ConjunctionOfClausesElementsFactory getInstance() {
		return instance;
	}
	
	/* --- Accessors --- */
	public DistributionStrategy<ConjunctionOfClauses, Clause> getDistributionStrategy() {
		return distributionStrategyToBeUsed;
	}

	public UnionStrategy<ConjunctionOfClauses, Clause> getUnionStrategy() {
		return unionStrategyToBeUsed;
	}
	
	public SubsumesStrategy<ConjunctionOfClauses, Clause> getSubsumesStrategy() {
		return subsumesStrategyToBeUsed;
	}
	
	public AddFormulaStrategy<ConjunctionOfClauses, Clause> getAddFormulaStrategy() {
		return addFormulaStrategy;
	}
	
	public RemoveFormulaStrategy<ConjunctionOfClauses, Clause> getRemoveFormulaStrategy() {
		return removeFormulaStrategy;
	}
	
	/* --- Mutators --- */
	public void setDistributionStrategy(DistributionStrategy<ConjunctionOfClauses, Clause> distributionStrategy) {
		this.distributionStrategyToBeUsed = distributionStrategy;
	}
	
	public void setAddFormulaStrategy(AddFormulaStrategy<ConjunctionOfClauses, Clause> addFormulaStrategy) {
		this.addFormulaStrategy = addFormulaStrategy;
	}
	
	public void setRemoveFormulaStrategy(RemoveFormulaStrategy<ConjunctionOfClauses, Clause> removeFormulaStrategy) {
		this.removeFormulaStrategy = removeFormulaStrategy;
	}
	
	public void setUnionStrategy(UnionStrategy<ConjunctionOfClauses, Clause> unionStrategy) {
		this.unionStrategyToBeUsed = unionStrategy;
	}
	
	public void setSubsumesStrategy(SubsumesStrategy<ConjunctionOfClauses, Clause> subsumesStrategy) {
		this.subsumesStrategyToBeUsed = subsumesStrategy;
	}
	
	/* --- Methods --- */
	/**
	 * Creates a propositional set based on the selected data type (default: LinkedHashSet ).
	 * @return propositional set
	 */
	public Set<Clause> createElement() {
		String dataType = StrategiesConfigurator.getInstance().getDataType();
		
		if (dataType.equals(StrategiesConfigurator.TRIE_TYPE_NAME))
			return new ClauseTrie();
		else if (dataType.equals(StrategiesConfigurator.HASHSET_TYPE_NAME))
			return new LinkedHashSet<Clause>();
		
		// Default result
		return new LinkedHashSet<Clause>();
	}
}
