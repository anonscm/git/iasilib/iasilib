package fr.lri.iasi.libs.plogic.restrictions;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fr.lri.iasi.libs.formallogic.Restriction;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.Literal;

/**
 * Describes a clause restriction by subset language.
 * 
 * @author Andre Fonseca
 *
 */
public class SubsetRestriction implements Restriction<Clause> {
	private static final long serialVersionUID = 3986808531617113565L;
	
	/* --- Properties --- */
	/** The subset language to use on the restriction */
	private final Set<String> languageSubSet;
	
	public SubsetRestriction() {
		this.languageSubSet = new HashSet<String>();
	}
	
	public SubsetRestriction(Set<Literal> languageSubSet) {
		if (languageSubSet == null) {
			throw new IllegalArgumentException();
		}
		
		this.languageSubSet = new HashSet<String>();
		for (Literal literal : languageSubSet) {
			this.languageSubSet.add(literal.getLabel());
		}
	}

	/* --- Accessors --- */
	@Override
	public String getName() {
		return "subset";
	}
	
	public Set<String> getSubSet() {
		return Collections.unmodifiableSet(languageSubSet);
	}
	
	/* --- Mutators --- */
	public boolean addToSubset(String literalName) {
		return languageSubSet.add(literalName);
	}
	
	public boolean removeFromSubset(Literal l) {
		return languageSubSet.remove(l.getLabel());
	}


	/* --- Methods --- */
	/**
     * {@inheritDoc}
     */
	@Override
	public boolean discard(Clause formula) {
		if (this.languageSubSet.isEmpty())
			return false;
		
		for (Literal literal : formula) {
			if (literal.canBeDiscarded() && !languageSubSet.contains(literal.getLabel()))
				return true;
		}
		
		return false;
	}
	
	/**
     * {@inheritDoc}
     */
	public boolean discard(Literal literal) {
		if (this.languageSubSet.isEmpty())
			return false;
		
		if (literal.canBeDiscarded() && !languageSubSet.contains(literal.getLabel()))
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
        
        for (String literalName : languageSubSet) {
        	res.append(" ");
        	res.append(literalName);
        }

        return res.toString();
    }
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (!(obj instanceof SubsetRestriction))
			return false;
		
		SubsetRestriction other = (SubsetRestriction) obj;
		if (!languageSubSet.equals(other.languageSubSet))
			return false;
		
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((languageSubSet == null) ? 0 : languageSubSet.hashCode());
		return result;
	}

}
