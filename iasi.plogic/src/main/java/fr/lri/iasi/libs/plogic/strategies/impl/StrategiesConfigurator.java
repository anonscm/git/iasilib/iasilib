package fr.lri.iasi.libs.plogic.strategies.impl;

import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.ConjunctionOfClauses;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.impl.ClauseElementsFactory;
import fr.lri.iasi.libs.plogic.impl.ConjunctionOfClausesElementsFactory;
import fr.lri.iasi.libs.plogic.impl.PropositionalClausalTheoryElementsFactory;

/**
 * Responsible for configure strategies used by the propositional elements
 * based on its selected data type.
 * 
 * @author Andre Fonseca
 *
 */
public final class StrategiesConfigurator {
	
	/* --- Constants --- */
	/** The name of the trie data type */
	public static final String TRIE_TYPE_NAME = "trie";
	
	/** The name of the hashSet data type */
	public static final String HASHSET_TYPE_NAME = "hashSet";
	
	/* --- Properties --- */
	/** The singleton object */
	private static StrategiesConfigurator instance = new StrategiesConfigurator();
	
	/** The default data type configuration parameter */
	private String dataType = StrategiesConfigurator.HASHSET_TYPE_NAME;
	
	private StrategiesConfigurator() {
	}
	
	public static StrategiesConfigurator getInstance() {
		return instance;
	}
	
	/* --- Accessors --- */
	public String getDataType() {
		return dataType;
	}
	
	/* --- Mutators --- */
	public void setDataType(String dataTypeStr) {
		this.dataType = dataTypeStr;
		
		configureStrategies(ClauseElementsFactory.getInstance());
		configureStrategies(PropositionalClausalTheoryElementsFactory.getInstance());
		configureStrategies(ConjunctionOfClausesElementsFactory.getInstance());
	}
	
	/* --- Methods --- */
	public void configureStrategies(ConjunctionOfClausesElementsFactory factory) {
		if (this.dataType.equalsIgnoreCase(StrategiesConfigurator.HASHSET_TYPE_NAME)) {
			
			factory.setUnionStrategy(new DefaultUnionStrategy<ConjunctionOfClauses, Clause>());
			factory.setDistributionStrategy(new DefaultDistributionStrategy<ConjunctionOfClauses, Clause>());
			factory.setSubsumesStrategy(new ClauseCollectionSubsumesStrategy<ConjunctionOfClauses>());
			factory.setAddFormulaStrategy(new ClauseCollectionAddFormulaStrategy());
			factory.setRemoveFormulaStrategy(new ClauseCollectionRemoveFormulaStrategy());
			
		} else if (this.dataType.equalsIgnoreCase(StrategiesConfigurator.TRIE_TYPE_NAME)) {
			
			factory.setUnionStrategy(new DefaultUnionStrategy<ConjunctionOfClauses, Clause>());
			factory.setDistributionStrategy(new DefaultDistributionStrategy<ConjunctionOfClauses, Clause>());
			factory.setSubsumesStrategy(new ClauseTrieSubsumesStrategy());
			factory.setAddFormulaStrategy(new ClauseTrieAddFormulaStrategy());
			factory.setRemoveFormulaStrategy(new ClauseTrieRemoveFormulaStrategy());
			
		}
	}
	
	public void configureStrategies(PropositionalClausalTheoryElementsFactory factory) {
		if (this.dataType.equalsIgnoreCase(StrategiesConfigurator.HASHSET_TYPE_NAME)) {
			
			factory.setAddFormulaStrategy(new TheoryAddFormulaStrategy());
			factory.setSubsumesStrategy(new TheoryIndexSubsumesStrategy());
			factory.setRemoveFormulaStrategy(new TheoryRemoveFormulaStrategy());
			
		} else if (this.dataType.equalsIgnoreCase(StrategiesConfigurator.TRIE_TYPE_NAME)) {
			
			factory.setAddFormulaStrategy(new TheoryAddFormulaStrategy());
			factory.setSubsumesStrategy(new TheoryDelegateSubsumesStrategy());
			factory.setRemoveFormulaStrategy(new TheoryTrieRemoveFormulaStrategy());
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public void configureStrategies(ClauseElementsFactory factory) {
		factory.setSubsumesStrategy(new DefaultSubsumesStrategy<Clause>());
		factory.setResolveStrategy(new ClauseResolveStrategy());
		factory.setUnionStrategy(new DefaultUnionStrategy<Clause, Literal>());
		factory.setDistributionStrategy(new UnsuportedDistributionStrategy());
	}

}
