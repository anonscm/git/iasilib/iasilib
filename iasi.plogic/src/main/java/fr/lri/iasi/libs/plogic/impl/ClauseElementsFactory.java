package fr.lri.iasi.libs.plogic.impl;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import fr.lri.iasi.libs.formallogic.Annotation;
import fr.lri.iasi.libs.plogic.Clause;
import fr.lri.iasi.libs.plogic.DistributionStrategy;
import fr.lri.iasi.libs.plogic.Literal;
import fr.lri.iasi.libs.plogic.UnionStrategy;
import fr.lri.iasi.libs.plogic.strategies.ResolveStrategy;
import fr.lri.iasi.libs.plogic.strategies.SubsumesStrategy;
import fr.lri.iasi.libs.plogic.strategies.impl.StrategiesConfigurator;

/**
 * Strategy factory. Responsible for the instantiation of the main strategies and
 * default annotations used by a clause.
 * 
 * @author Andre Fonseca
 *
 */
public final class ClauseElementsFactory {
	
	/* --- Properties --- */
	/** The singleton object */
	private static ClauseElementsFactory instance = new ClauseElementsFactory();
	
	/** The default annotations to be create within any clause */
	private final Set<Annotation<?, ?>> defaultAnnotations;
	
	/** The subsumption strategy to be used by all clauses */
	private SubsumesStrategy<Clause, Clause> subsumesStrategyToBeUsed;
	
	/** The resolve strategy to be used by all clauses */
	private ResolveStrategy<Clause, Literal> resolveStrategyToBeUsed;
	
	/** The union strategy to be used by all clauses */
	private UnionStrategy<Clause, Literal> unionStrategyToBeUsed;
	
	/** The distribution strategy to be used by all clauses */
	private DistributionStrategy<Clause, Literal> distributionStrategyToBeUsed;
	
	private ClauseElementsFactory() {
		this.defaultAnnotations = new LinkedHashSet<Annotation<?, ?>>();
		StrategiesConfigurator.getInstance().configureStrategies(this);
	}
	
	public static ClauseElementsFactory getInstance() {
		return instance;
	}
	
	/* --- Accessors --- */
	public ResolveStrategy<Clause, Literal> getResolveStrategy() {
		return resolveStrategyToBeUsed;
	}

	public SubsumesStrategy<Clause, Clause> getSubsumesStrategy() {
		return subsumesStrategyToBeUsed;
	}

	public DistributionStrategy<Clause, Literal> getDistributionStrategy() {
		return distributionStrategyToBeUsed;
	}

	public UnionStrategy<Clause, Literal> getUnionStrategy() {
		return unionStrategyToBeUsed;
	}
	
	public Set<Annotation<?, ?>> getAnnotations() {
		return Collections.unmodifiableSet(this.defaultAnnotations);
	}
	
	/* --- Mutators --- */
	public void setResolveStrategy(ResolveStrategy<Clause, Literal> resolveStrategy) {
		this.resolveStrategyToBeUsed = resolveStrategy;
	}
	
	public void setSubsumesStrategy(SubsumesStrategy<Clause, Clause> subsumesStrategy) {
		this.subsumesStrategyToBeUsed = subsumesStrategy;
	}
	
	public void setDistributionStrategy(DistributionStrategy<Clause, Literal> distributionStrategy) {
		this.distributionStrategyToBeUsed = distributionStrategy;
	}
	
	public void setUnionStrategy(UnionStrategy<Clause, Literal> unionStrategy) {
		this.unionStrategyToBeUsed = unionStrategy;
	}
	
	/* --- Methods --- */
	public void addAnnotations(Annotation<?, ?> annotation) {
		this.defaultAnnotations.add(annotation);
	}

}
